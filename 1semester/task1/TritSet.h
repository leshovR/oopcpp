#pragma once
#include <memory>
#include <unordered_map>
#include <string>

enum class TritValue { Unknown, True, False };


/* TritValue operations */

const TritValue operator~(const TritValue val);

const TritValue operator&(const TritValue lhs, const TritValue rhs);

TritValue& operator&=(TritValue& lhs, const TritValue rhs);

const TritValue operator|(const TritValue lhs, const TritValue rhs);

TritValue& operator|=(TritValue& lhs, const TritValue rhs);

std::ostream& operator<<(std::ostream& out, const TritValue val);


/* 
	TritSet - container for dynamic-length 
	sequences of trits   
*/
class TritSet
{
public:

	// Proxy for an element
	class Reference
	{
		friend TritSet;
	public:
		// Destroying the object
		~Reference();

		// Assign TritValue to element
		Reference& operator=(TritValue val);

		// Assign reference to element
		Reference& operator=(const Reference& ref);

		// Return value of element
		operator TritValue() const;

	private:
		// Default construct
		Reference();

		// Construct from TritSet and position
		Reference(TritSet* set, size_t position);

		TritSet* tritset; // pointer to the tritset
		size_t pos;		  // position of element in tritset
	};


	TritSet();

	TritSet(size_t size);
	
	TritSet(const TritSet&);
	TritSet(TritSet&&);

	~TritSet();


	unsigned* firstWord() const;

	unsigned* lastWord() const;

	// Max number of trits
	size_t capacity() const;

	// Length of unsigned int array
	size_t wordCount() const;

	// Most significant trit index
	size_t length() const;

	size_t cardinality(TritValue val) const;


	void shrink();

	// Clear all trits from lastIndex
	void trim(size_t lastIndex);

	const TritValue getTrit(size_t pos) const;


	const TritValue operator[](size_t pos) const;


	Reference operator[](size_t pos);


	TritSet& setTrit(size_t pos, TritValue val);

	
	TritSet& operator=(const TritSet&);

	TritSet& operator=(TritSet&&);
	
	TritSet& operator&=(const TritSet& rhs);
	
	TritSet& operator|=(const TritSet& rhs);
	
	TritSet& operator~();
	
private:

	std::pair<unsigned*, unsigned*> 
		alloc_n_copy(const unsigned* b, const unsigned* e);

	void chk_n_alloc(size_t pos);

	void free();

	void reallocate(size_t minSize);

		
	const unsigned tritsPerWord = 4 * sizeof(unsigned);
	
	
	unsigned* elements;
	unsigned* cap;
};


std::ostream& operator<<(std::ostream& os, const TritSet& set);

TritSet operator&(const TritSet& lhs, const TritSet& rhs);

TritSet operator|(const TritSet& lhs, const TritSet& rhs);
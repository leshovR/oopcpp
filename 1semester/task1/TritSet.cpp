#include "pch.h"
#include "TritSet.h"

using namespace std;

/* Trit operations */

const TritValue operator~(const TritValue val)
{
	if (val == TritValue::True)
		return TritValue::False;
	else if (val == TritValue::False)
		return TritValue::True;
	else 
		return TritValue::Unknown;
}


const TritValue operator&(const TritValue lhs, const TritValue rhs)
{
	if (lhs == TritValue::False || rhs == TritValue::False)
		return TritValue::False;

	else if (lhs == TritValue::Unknown || rhs == TritValue::Unknown)
		return TritValue::Unknown;

	return TritValue::True;
}


TritValue& operator&=(TritValue& lhs, const TritValue rhs)
{
	lhs = lhs & rhs;
	return lhs;
}


const TritValue operator|(const TritValue lhs, const TritValue rhs)
{
	if (lhs == TritValue::True || rhs == TritValue::True)
		return TritValue::True;

	else if (lhs == TritValue::Unknown || rhs == TritValue::Unknown)
		return TritValue::Unknown;

	return TritValue::False;
}


TritValue& operator|=(TritValue& lhs, const TritValue rhs)
{
	lhs = lhs | rhs;
	return lhs;
}


ostream& operator<<(ostream& out, const TritValue val)
{
	if (val == TritValue::False)
		out << "0";
	else if (val == TritValue::True)
		out << "1";
	else if (val == TritValue::Unknown)
		out << "?";
	else out << "Invalid TritValue";

	return out;
}


/* REFERENCE */

TritSet::Reference::Reference() : tritset(nullptr), pos(0)
{

}


TritSet::Reference::Reference(TritSet* set, size_t position) :
	tritset(set), pos(position)
{

}


TritSet::Reference::~Reference()
{

}


TritSet::Reference::operator TritValue() const
{
	return tritset->getTrit(pos);
}


TritSet::Reference& TritSet::Reference::operator=(TritValue val)
{
	tritset->setTrit(pos, val);
	return (*this);
}


TritSet::Reference& TritSet::Reference::operator=(const Reference& ref)
{
	tritset->setTrit(pos, TritValue(ref));
	return (*this);
}


/* TRITSET */


TritSet::TritSet() :
	elements(nullptr), cap(nullptr)
{

}


TritSet::TritSet(size_t trits) :
	elements(nullptr), cap(nullptr)
{
	size_t words = trits / tritsPerWord + 1;
	elements = (new unsigned[words + 1]{ 0 });
	cap = elements + words;
}


TritSet::TritSet(const TritSet& set) 
{
	auto newdata = alloc_n_copy(set.firstWord(), set.lastWord());
	elements = newdata.first;
	cap = newdata.second;
}


TritSet::TritSet(TritSet&& set) : elements(set.elements), 
	cap(set.cap)
{
	set.elements = set.cap = nullptr;
}




TritSet::~TritSet()
{
	free();
}


void TritSet::free()
{
	if (elements)
		delete[] elements;
}


unsigned* TritSet::firstWord() const
{
	return elements;
}


unsigned* TritSet::lastWord() const
{
	return cap;
}


size_t TritSet::capacity() const
{
	return (wordCount() * tritsPerWord);
}


size_t TritSet::wordCount() const
{
	return cap - elements;
}


size_t TritSet::length() const
{
	if (capacity() == 0) return 0;
	auto itr = lastWord() - 1;
	for (itr; itr != firstWord() - 1 && !(*itr); --itr)
	{
		/* empty */
	}

	size_t pos = (itr - elements + 1) * tritsPerWord;
	
	for (size_t i = pos + 1; i != pos - tritsPerWord; /* empty */)
	{
		if (getTrit(--i) != TritValue::Unknown)
			return i;
	}

	return 0;
}


size_t TritSet::cardinality(TritValue val) const
{
	if (val == TritValue::Unknown)
	{
		return capacity() - length();
	}

	size_t max = length();
	size_t count = 0;
	for (size_t i = 0; i != wordCount(); ++i)
	{
		if (elements[i] != 0)
		{
			for (size_t j = 0; j != tritsPerWord; ++j)
			{
				TritValue cur = getTrit(j);
				if (cur == val) ++count;
			}
		}
	}

	return count;
}


void TritSet::shrink()
{
	size_t newsize = length() / tritsPerWord + 1;
	
	cap = elements + newsize;

	reallocate(newsize);

}


void TritSet::trim(size_t lastIndex)
{
	if (lastIndex >= capacity())
		return;

	size_t pos = lastIndex / tritsPerWord;
	size_t last = (pos + 1) * tritsPerWord;
	for (size_t i = lastIndex; i != last; ++i)
	{
		setTrit(i, TritValue::Unknown);
	}

	for (size_t i = pos + 1; i != wordCount(); ++i)
	{
		elements[i] = 0;
	}
}


void TritSet::chk_n_alloc(size_t pos)
{
	if (pos >= capacity())
		reallocate(pos);
}


void TritSet::reallocate(size_t newSize)
{
	auto first = new unsigned[newSize + 1] {0};
	auto dest = first;
	auto elem = elements;

	for (size_t i = 0; i != wordCount(); ++i)
	{
		*dest = *elem++;
		++dest;
	}

	free();

	elements = first;
	cap = first + newSize;

}


std::pair<unsigned*, unsigned*>
	TritSet::alloc_n_copy(const unsigned* b, const unsigned* e)
{
	auto data = new unsigned[e - b + 1]{ 0 };
	return { data, copy(b, e, data) };
}


const TritValue TritSet::getTrit(size_t pos) const
{
	if (pos > capacity()) return TritValue::Unknown;

	unsigned word = elements[pos / tritsPerWord];
	unsigned offset = (pos % tritsPerWord) * 2;
	unsigned mask = (3 << offset);

	unsigned trit = (word & mask) >> offset;

	return static_cast<TritValue>(trit);
}


TritSet& TritSet::setTrit(size_t pos, TritValue val)
{
	unsigned position = pos / tritsPerWord;

	if (position >= wordCount())
	{
		if (val == TritValue::Unknown)
			return (*this);
		else
			reallocate(position + 1);
	}

	unsigned offset = (pos % tritsPerWord) * 2;
	unsigned mask = 3 << offset;
	unsigned value = static_cast<unsigned>(val);
	elements[position] &= ~mask;
	if (val != TritValue::Unknown)
		elements[position] |= (value << offset);

	
	return (*this);
}

const TritValue TritSet::operator[](size_t pos) const
{
	if (pos < capacity())
		return getTrit(pos);
	else
		return TritValue::Unknown;
}


TritSet::Reference TritSet::operator[](size_t pos)
{
	return Reference(this, pos);
}


TritSet& TritSet::operator=(const TritSet& rhs)
{
	if (this == &rhs)
		return (*this);
	auto data = alloc_n_copy(rhs.firstWord(), rhs.lastWord());
	free();
	elements = data.first;
	cap = data.second;

	return (*this);
}


TritSet& TritSet::operator=(TritSet&& rhs)
{
	if (this != &rhs)
	{
		free();
	}

	elements = rhs.elements;
	cap = rhs.cap;

	// leave in the destructible state
	rhs.elements = rhs.cap = nullptr;

	return (*this);
}


TritSet& TritSet::operator&=(const TritSet& rhs)
{
	if (wordCount() < rhs.wordCount())
		reallocate(rhs.wordCount());

	for (size_t i = 0; i != capacity(); ++i)
	{
		setTrit(i, getTrit(i) & rhs.getTrit(i));
	}
	
	return (*this);
}


TritSet& TritSet::operator|=(const TritSet& rhs)
{
	if (wordCount() < rhs.wordCount())
		reallocate(rhs.wordCount());

	for (size_t i = 0; i != capacity(); ++i)
	{
		setTrit(i, getTrit(i) | rhs.getTrit(i));
	}

	return (*this);
}

TritSet& TritSet::operator~()
{
	for (size_t i = 0; i != capacity(); ++i)
	{
		setTrit(i, ~getTrit(i));
	}

	return (*this);
}

std::ostream& operator<<(std::ostream& os, const TritSet& set)
{
	for (size_t i = set.capacity(); i > 0; /* empty */)
	{
		os << set.getTrit(--i);
	}

	return os;
}


TritSet operator&(const TritSet& lhs, const TritSet& rhs)
{
	TritSet ans = lhs;
	ans &= rhs;
	return ans;
}


TritSet operator|(const TritSet& lhs, const TritSet& rhs)
{
	TritSet ans = lhs;
	ans |= rhs;
	return ans;
}
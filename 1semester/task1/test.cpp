#include "pch.h"

#include "TritSet.h"


TEST(TritTest, IsEmptyInitially)
{
	TritSet ts;
	EXPECT_EQ(0, ts.wordCount());
	EXPECT_EQ(0, ts.capacity());
	EXPECT_EQ(0, ts.length());
}

TEST(TritTest, IsInitializationCorrect)
{
	TritSet ts(1000);
	EXPECT_LE(1000 * 2 / 8 / sizeof(unsigned), ts.capacity());
	EXPECT_NE(0, ts.wordCount());
	EXPECT_NE(0, ts.capacity());
	EXPECT_EQ(0, ts.length());
}

TEST(TritTest, IsCopyingCorrect)
{
	TritSet a(10), b;
	a[5] = TritValue::True;
	b = a;

	EXPECT_EQ(a.capacity(), b.capacity());
	EXPECT_EQ(a.length(), b.length());
	EXPECT_EQ(TritValue::True, b[5]);

	a = a;

	EXPECT_EQ(a.capacity(), b.capacity());
	EXPECT_EQ(a.length(), b.length());
	EXPECT_EQ(TritValue::True, a[5]);
}

TEST(TritTest, IsMovingCorrect)
{
	TritSet a(10), b;

	a[5] = TritValue::True;
	size_t cap = a.capacity();
	size_t len = a.length();

	b = std::move(a);
	EXPECT_EQ(TritValue::True, b[5]);
	EXPECT_EQ(cap, b.capacity());
	EXPECT_EQ(len, b.length());
}

TEST(TritTest, IsGetTritCorrect)
{
	TritSet ts(1000);
	EXPECT_EQ(TritValue::Unknown, ts[50]);
	EXPECT_EQ(TritValue::Unknown, ts.getTrit(50));
	ts[50] = TritValue::True;
	EXPECT_EQ(ts.getTrit(50), ts[50]);
}

TEST(TritTest, IsSetTritCorrect)
{
	TritSet ts(1000);
	ts.setTrit(100, TritValue::False);
	EXPECT_EQ(ts.getTrit(100), TritValue::False);
	ts[150] = TritValue::True;
	EXPECT_EQ(ts.getTrit(150), TritValue::True);
	ts[150] = TritValue::Unknown;
	EXPECT_EQ(ts.getTrit(150), TritValue::Unknown);
}

TEST(TritTest, SetTritWithAlloc)
{
	TritSet ts;
	auto sz = ts.capacity();
	EXPECT_EQ(sz, 0);
	ts[100] = TritValue::True;
	EXPECT_LT(sz, ts.capacity());
	sz = ts.capacity();
	ts[sz] = TritValue::False;
	EXPECT_LT(sz, ts.capacity());
}

TEST(TritTest, SetTritNoAlloc)
{
	TritSet ts(1000);
	auto sz = ts.capacity();
	ts[sz + 100] = TritValue::Unknown;
	EXPECT_EQ(sz, ts.capacity());
	ts[sz - 1] = TritValue::False;
	EXPECT_EQ(sz, ts.capacity());
}

TEST(TritTest, GetTritNoAlloc)
{
	TritSet ts;
	size_t sz = ts.capacity();
	if (ts.getTrit(10000) == TritValue::True)
		/* Empty */;
	EXPECT_EQ(ts.capacity(), sz);
	if (ts[10000] == TritValue::False)
		/* Empty */;
	EXPECT_EQ(ts.capacity(), sz);
}

TEST(TritTest, IsAndCorrect)
{
	TritSet a, b;
	a[0] = TritValue::True;
	b[0] = TritValue::True;

	a[1] = TritValue::True;
	b[1] = TritValue::False;

	a[2] = TritValue::True;
	b[2] = TritValue::Unknown;

	a[3] = TritValue::False;
	b[3] = TritValue::Unknown;

	a[4] = TritValue::False;
	b[4] = TritValue::False;

	a[5] = TritValue::Unknown;
	b[5] = TritValue::Unknown;

	TritSet c = a & b;
	EXPECT_EQ(TritValue::True, c[0]);
	EXPECT_EQ(TritValue::False, c[1]);
	EXPECT_EQ(TritValue::Unknown, c[2]);
	EXPECT_EQ(TritValue::False, c[3]);
	EXPECT_EQ(TritValue::False, c[4]);
	EXPECT_EQ(TritValue::Unknown, c[5]);
}

TEST(TritTest, IsOrCorrect)
{
	TritSet a, b;
	a[0] = TritValue::True;
	b[0] = TritValue::True;

	a[1] = TritValue::True;
	b[1] = TritValue::False;

	a[2] = TritValue::True;
	b[2] = TritValue::Unknown;

	a[3] = TritValue::False;
	b[3] = TritValue::Unknown;

	a[4] = TritValue::False;
	b[4] = TritValue::False;

	a[5] = TritValue::Unknown;
	b[5] = TritValue::Unknown;

	TritSet c = a | b;
	EXPECT_EQ(TritValue::True, c[0]);
	EXPECT_EQ(TritValue::True, c[1]);
	EXPECT_EQ(TritValue::True, c[2]);
	EXPECT_EQ(TritValue::Unknown, c[3]);
	EXPECT_EQ(TritValue::False, c[4]);
	EXPECT_EQ(TritValue::Unknown, c[5]);
}

TEST(TritTest, IsCapacityCorrect)
{
	TritSet ts;
	EXPECT_EQ(0, ts.capacity());
	ts[10 * sizeof(unsigned)] = TritValue::True;
	EXPECT_EQ(12 * sizeof(unsigned), ts.capacity());
	ts[1000] = TritValue::False;
	size_t sz = (1000 % (8*sizeof(unsigned))) + 1000;
	EXPECT_EQ(sz, ts.capacity());
}

TEST(TritTest, IsWordCountCorrect)
{
	TritSet ts;
	EXPECT_EQ(0, ts.wordCount());
	ts[1000] = TritValue::True;
	size_t sz = 1000 / (4 * sizeof(unsigned)) + 1;
	EXPECT_EQ(sz, ts.wordCount());
}

TEST(TritTest, IsLengthCorrect)
{
	TritSet ts(2000);
	EXPECT_EQ(0, ts.length());
	ts[1000] = TritValue::True;
	EXPECT_EQ(1000, ts.length());

	ts[100] = TritValue::False;
	ts[1000] = TritValue::Unknown;
	EXPECT_EQ(100, ts.length());

	ts[101] = TritValue::True;
	EXPECT_EQ(101, ts.length());
}

TEST(TritTest, IsNotCorrect)
{
	TritSet ts(3);
	ts[0] = TritValue::False;
	ts[1] = TritValue::True;

	ts = ~ts;
	EXPECT_EQ(TritValue::True, ts[0]);
	EXPECT_EQ(TritValue::False, ts[1]);
	EXPECT_EQ(TritValue::Unknown, ts[2]);
}

TEST(TritTest, OperationAlloc)
{
	TritSet a(1000), b(2000);
	TritSet c = a & b;
	EXPECT_EQ(c.capacity(), b.capacity());
	TritSet d = a | b;
	EXPECT_EQ(d.capacity(), b.capacity());
}

TEST(TritTest, IsTrimCorrect)
{
	TritSet ts(100);
	ts[10] = TritValue::True;
	ts[11] = TritValue::True;
	ts[80] = TritValue::True;
	ts[99] = TritValue::True;

	ts.trim(11);
	EXPECT_EQ(TritValue::True, ts[10]);
	EXPECT_EQ(TritValue::Unknown, ts[11]);
	EXPECT_EQ(TritValue::Unknown, ts[80]);
	EXPECT_EQ(TritValue::Unknown, ts[99]);
	EXPECT_EQ(TritValue::Unknown, ts[20]);
}

TEST(TritTest, IsCardinalityCorrect)
{
	TritSet ts(10);
	ts[1] = ts[3] = ts[5] = TritValue::True;
	ts[0] = ts[2] = ts[4] = ts[6] = TritValue::False;
	EXPECT_EQ(3, ts.cardinality(TritValue::True));
	EXPECT_EQ(4, ts.cardinality(TritValue::False));
	EXPECT_EQ(ts.capacity() - 6, ts.cardinality(TritValue::Unknown));
}

TEST(TritTest, IsShrinkCorrect)
{
	TritSet ts;
	ts[1000] = TritValue::True;
	ts[100] = TritValue::True;

	size_t cap = ts.capacity();

	ts[1000] = TritValue::Unknown;
	EXPECT_EQ(cap, ts.capacity());
	ts.shrink();
	EXPECT_GT(cap, ts.capacity());
}


int main(int argc, char** argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
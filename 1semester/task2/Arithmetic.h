#pragma once

#include "Instruction.h"

class Add : public Instruction
{
public:

	Add(ExecutionContext* cxt);

	void doStuff(std::vector<std::string>* args) override;

private:


};


class Sub : public Instruction
{
public:

	Sub(ExecutionContext* cxt);


	void doStuff(std::vector<std::string>* args) override;

private:


};


class Mul : public Instruction
{
public:

	Mul(ExecutionContext* cxt);


	void doStuff(std::vector<std::string>* args) override;

private:


};


class Div : public Instruction
{
public:

	Div(ExecutionContext* cxt);


	void doStuff(std::vector<std::string>* args) override;

private:


};


class Sqrt : public Instruction
{
public:

	Sqrt(ExecutionContext* cxt);


	void doStuff(std::vector<std::string>* args) override;

private:


};
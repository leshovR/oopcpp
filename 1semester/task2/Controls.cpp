#include "pch.h"
#include "Controls.h"
#include "CalcException.h"
#include <iostream>

using namespace std;


bool isName(const string& str)
{
	if (!isalpha(str[0]))
		return false;
	for (auto c : str)
	{
		if (!isalnum(c)) return false;
	}

	return true;
}


bool isDouble(const string& str)
{
	try
	{
		std::stod(str);
	}
	catch (...)
	{
		return false;
	}
	return true;
}


Push::Push(ExecutionContext* cxt) :
	Instruction(cxt)
{

}


void Push::doStuff(vector<string>* args)
{
	if (!args || args->size() == 0) throw ArgumentError("Not enough args");

	for (auto itr : *args)
	{
		if (isDouble(itr))
		{
			double number = atof(itr.c_str());
			context->st.push(number);
		}
		else if (isName(itr))
		{
			auto item = context->table.find(itr);
			if (item == context->table.end())
				throw ArgumentError("Bad argument");
			double meaning = item->second;
			context->st.push(meaning);
		}
		else
			throw ArgumentError("Bad argument");
	}
}




Define::Define(ExecutionContext* cxt) :
	Instruction(cxt)
{

}


void Define::doStuff(vector<string>* args)
{
	if (!args || args->size() != 2) throw ArgumentError("Bad arguments");

	string name = (*args)[0];
	string definition = (*args)[1];

	if (!isName(name)) throw ArgumentError(name + " is not valid identifier");

	if (!isDouble(definition)) throw ArgumentError(definition +
		" is not valid value");

	double meaning = atof(definition.c_str());

	context->table[name] = meaning;
}


Pop::Pop(ExecutionContext* cxt) :
	Instruction(cxt)
{

}


void Pop::doStuff(vector<string>* args)
{
	if (context->st.size() == 0)
		throw StackError("Nothing to pop");
	if (!args || args->size() == 0)
	{
		context->st.pop();
		return;
	}

	if (context->st.size() < args->size())
		throw StackError("Can't pop this much");

	for (auto name : (*args))
	{
		double definition = context->st.top();
		context->st.pop();
		context->table[name] = definition;
	}

}


Print::Print(ExecutionContext* cxt) :
	Instruction(cxt)
{

}


void Print::doStuff(vector<string>* args)
{
	if (context->st.size() < 1)
		throw StackError("Nothing to print");
	if (args->size() != 0)
		throw ArgumentError("Wrong usage of PRINT");

	double definition = context->st.top();

	context->os << definition << endl;
}


Comment::Comment(ExecutionContext* cxt) :
	Instruction(cxt)
{

}


void Comment::doStuff(vector<string>* args)
{

}
#include "pch.h"
#include "InstructionFactory.h"


using namespace std;

InstructionFactory::InstructionFactory(ExecutionContext* cxt) :
	context(cxt)
{

}

Instruction* InstructionFactory::createInst(const string& name)
{
	auto registeredPair = registeredNames.find(name);

	if (registeredPair == registeredNames.end())
		return nullptr;

	return registeredPair->second();
}
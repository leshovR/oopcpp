#pragma once

#include "Instruction.h"
#include "Arithmetic.h"
#include "Controls.h"
#include "InstructionFactory.h"
#include <iostream>

class Calculator
{
public:

	Calculator(std::istream& is, std::ostream& os);

	void runProgram();


private:
	void initFactory(InstructionFactory& factory);

	ExecutionContext context;
	std::istream& in;

	InstructionFactory factory;

};
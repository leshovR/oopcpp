#include "pch.h"
#include "Arithmetic.h"
#include "CalcException.h"
#include <limits>

using namespace std;

Add::Add(ExecutionContext* cxt) :
	Instruction(cxt)
{

}


void Add::doStuff(vector<string>* args)
{
	if (args && args->size() != 0)
		throw ArgumentError("Wrong usage of ADD command");
	if (context->st.size() < 2)
		throw StackError("Not enough args in stack to add");
	
	double a = context->st.top();
	context->st.pop();
	double b = context->st.top();
	context->st.pop();

	context->st.push(a + b);
}


Sub::Sub(ExecutionContext* cxt) :
	Instruction(cxt)
{

}


void Sub::doStuff(vector<string>* args)
{
	if (args && args->size() != 0)
		throw ArgumentError("Wrong usage of SUB");
	if (context->st.size() < 2)
		throw StackError("Not enough args in stack to subtract");
	
	double a = context->st.top();
	context->st.pop();
	double b = context->st.top();
	context->st.pop();

	context->st.push(b - a);
}


Mul::Mul(ExecutionContext* cxt) :
	Instruction(cxt)
{

}


void Mul::doStuff(vector<string>* args)
{
	if (args && args->size() != 0)
		throw ArgumentError("Wrong usage of MUL");
	if (context->st.size() < 2)
		throw StackError("Not enough args in stack to multiply");
	
	double a = context->st.top();
	context->st.pop();
	double b = context->st.top();
	context->st.pop();

	context->st.push(a * b);
}


Div::Div(ExecutionContext* cxt) :
	Instruction(cxt)
{

}


void Div::doStuff(vector<string>* args)
{
	if (args && args->size() != 0)
		throw ArgumentError("Wrong usage of DIV");
	if (context->st.size() < 2)
		throw StackError("Not enough args in stack to divide");
	
	double a = context->st.top();
	context->st.pop();
	double b = context->st.top();
	context->st.pop();

	context->st.push(b / a);
}


Sqrt::Sqrt(ExecutionContext* cxt) :
	Instruction(cxt)
{

}


void Sqrt::doStuff(vector<string>* args)
{
	if (args && args->size() != 0)
		throw ArgumentError("Wrong usage of SQRT");
	if (context->st.size() < 1)
		throw StackError("Not enough args in stack to take square root");
	
	double a = context->st.top();
	context->st.pop();
	
	if (a < 0) throw ArgumentError("Invalid argument");

	context->st.push(sqrt(a));
}
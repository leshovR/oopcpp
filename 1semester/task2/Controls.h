#pragma once
#include "Instruction.h"

class Push : public Instruction
{
public:
	Push(ExecutionContext* cxt);


	void doStuff(std::vector<std::string>* args) override;


private:

};


class Define : public Instruction
{
public:
	Define(ExecutionContext* cxt);


	void doStuff(std::vector<std::string>* args) override;

private:

};


class Pop : public Instruction
{
public:
	Pop(ExecutionContext* cxt);

	void doStuff(std::vector<std::string>* args) override;

private:

};


class Print : public Instruction
{
public:

	Print(ExecutionContext* cxt);


	void doStuff(std::vector<std::string>* args) override;

private:

};


class Comment : public Instruction
{
public:
	Comment(ExecutionContext* cxt);


	void doStuff(std::vector<std::string>* args) override;


private:
};
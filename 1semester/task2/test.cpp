#include "pch.h"
#include "Calculator.h"
#include <iostream>
#include "CalcException.h"

TEST(CalcTest, AddTest) 
{
	ExecutionContext cxt(std::cout);
	Add op(&cxt);

	cxt.st.push(4.5);
	cxt.st.push(3.2);
	op.doStuff(nullptr);

	EXPECT_DOUBLE_EQ(cxt.st.top(), 7.7);

	cxt.st.push(-4.5);
	cxt.st.push(-3.2);
	op.doStuff(nullptr);

	EXPECT_DOUBLE_EQ(cxt.st.top(), -7.7);

	op.doStuff(nullptr);

	EXPECT_DOUBLE_EQ(cxt.st.top(), 0.0);
}


TEST(CalcTest, AddTestNegative)
{
	ExecutionContext cxt(std::cout);
	Add op(&cxt);

	EXPECT_THROW(op.doStuff(nullptr), StackError);

	cxt.st.push(-1);
	
	EXPECT_THROW(op.doStuff(nullptr), StackError);

	std::vector<std::string> args = { "5.23, 12" };

	EXPECT_THROW(op.doStuff(&args), ArgumentError);
}


TEST(CalcTest, SubTest)
{
	ExecutionContext cxt(std::cout);
	Sub op(&cxt);

	cxt.st.push(4.5);
	cxt.st.push(3.2);
	op.doStuff(nullptr);

	EXPECT_DOUBLE_EQ(cxt.st.top(), 1.3);

	cxt.st.push(-4.5);
	cxt.st.push(-3.2);
	op.doStuff(nullptr);

	EXPECT_DOUBLE_EQ(cxt.st.top(), -1.3);

	op.doStuff(nullptr);

	EXPECT_DOUBLE_EQ(cxt.st.top(), 2.6);
}


TEST(CalcTest, SubTestNegative)
{
	ExecutionContext cxt(std::cout);
	Sub op(&cxt);

	EXPECT_THROW(op.doStuff(nullptr), StackError);

	cxt.st.push(-1);

	EXPECT_THROW(op.doStuff(nullptr), StackError);

	std::vector<std::string> args = { "5.23, 12" };

	EXPECT_THROW(op.doStuff(&args), ArgumentError);
}


TEST(CalcTest, MulTest)
{
	ExecutionContext cxt(std::cout);
	Mul op(&cxt);

	cxt.st.push(4.5);
	cxt.st.push(3.2);
	op.doStuff(nullptr);

	EXPECT_DOUBLE_EQ(cxt.st.top(), 14.4);

	cxt.st.push(-4.5);
	cxt.st.push(-3.2);
	op.doStuff(nullptr);

	EXPECT_DOUBLE_EQ(cxt.st.top(), 14.4);


	cxt.st.push(-4.5);
	cxt.st.push(3.2);
	op.doStuff(nullptr);

	EXPECT_DOUBLE_EQ(cxt.st.top(), -14.4);

}


TEST(CalcTest, MulTestNegative)
{
	ExecutionContext cxt(std::cout);
	Mul op(&cxt);

	EXPECT_THROW(op.doStuff(nullptr), StackError);

	cxt.st.push(-1);

	EXPECT_THROW(op.doStuff(nullptr), StackError);

	std::vector<std::string> args = { "5.23, 12" };

	EXPECT_THROW(op.doStuff(&args), ArgumentError);
}





TEST(CalcTest, DivTest)
{
	ExecutionContext cxt(std::cout);
	Div op(&cxt);

	cxt.st.push(4.5);
	cxt.st.push(3.2);
	op.doStuff(nullptr);

	EXPECT_DOUBLE_EQ(cxt.st.top(), 1.40625);

	cxt.st.push(-4.5);
	cxt.st.push(-3.2);
	op.doStuff(nullptr);

	EXPECT_DOUBLE_EQ(cxt.st.top(), 1.40625);

	cxt.st.push(4.5);
	cxt.st.push(-3.2);
	op.doStuff(nullptr);

	EXPECT_DOUBLE_EQ(cxt.st.top(), -1.40625);
}


TEST(CalcTest, DivTestNegative)
{
	ExecutionContext cxt(std::cout);
	Div op(&cxt);

	EXPECT_THROW(op.doStuff(nullptr), StackError);

	cxt.st.push(-1);

	EXPECT_THROW(op.doStuff(nullptr), StackError);

	std::vector<std::string> args = { "5.23, 12" };

	EXPECT_THROW(op.doStuff(&args), ArgumentError);
}



TEST(CalcTest, SqrtTest)
{
	ExecutionContext cxt(std::cout);
	Sqrt op(&cxt);

	cxt.st.push(6.25);
	op.doStuff(nullptr);

	EXPECT_DOUBLE_EQ(cxt.st.top(), 2.5);

	cxt.st.push(201.9241);
	op.doStuff(nullptr);

	EXPECT_DOUBLE_EQ(cxt.st.top(), 14.21);
}


TEST(CalcTest, SqrtTestNegative)
{
	ExecutionContext cxt(std::cout);
	Sqrt op(&cxt);

	EXPECT_THROW(op.doStuff(nullptr), StackError);

	cxt.st.push(-1);

	EXPECT_THROW(op.doStuff(nullptr), ArgumentError);
	
	std::vector<std::string> args = { "5.23, 12" };

	EXPECT_THROW(op.doStuff(&args), ArgumentError);
}


TEST(CalcTest, DefineTest)
{
	ExecutionContext cxt(std::cout);
	Define op(&cxt);

	std::vector<std::string> args = { "a", "24.3" };
	
	op.doStuff(&args);
	
	auto item = cxt.table.find("a");
	EXPECT_NE(item, cxt.table.end());
	EXPECT_DOUBLE_EQ(item->second, 24.3);

}


TEST(CalcTest, DefineNegativeTest)
{
	ExecutionContext cxt(std::cout);
	Define op(&cxt);

	EXPECT_THROW(op.doStuff(nullptr), ArgumentError);
	std::vector<std::string> args;

	EXPECT_THROW(op.doStuff(&args), ArgumentError);
	
	args.push_back("a");
	EXPECT_THROW(op.doStuff(&args), ArgumentError);
	args.push_back("24.3");
	args.push_back("11");
	EXPECT_THROW(op.doStuff(&args), ArgumentError);

	args.pop_back();
	args[0] = "12.6";
	EXPECT_THROW(op.doStuff(&args), ArgumentError);
	args[0] = "a";
	args[1] = "b";
	EXPECT_THROW(op.doStuff(&args), ArgumentError);
}


TEST(CalcTest, PushTest)
{
	ExecutionContext cxt(std::cout);
	Push op(&cxt);
	std::vector<std::string> args;

	args.emplace_back("1.05");
	op.doStuff(&args);

	EXPECT_DOUBLE_EQ(cxt.st.top(), 1.05);
	EXPECT_EQ(cxt.st.size(), 1);

	args.push_back("24.2");
	args.push_back("0.76");
	args.push_back("13.71");
	op.doStuff(&args);
	
	EXPECT_DOUBLE_EQ(cxt.st.top(), 13.71);
	EXPECT_EQ(cxt.st.size(), 5);

	cxt.table["a"] = 12.31;
	args.push_back("a");
	op.doStuff(&args);
	
	EXPECT_DOUBLE_EQ(cxt.st.top(), 12.31);
	EXPECT_EQ(cxt.st.size(), 10);
}


TEST(CalcTest, PushNegativeTest)
{
	ExecutionContext cxt(std::cout);
	Push op(&cxt);
	std::vector<std::string> args;

	EXPECT_THROW(op.doStuff(nullptr), ArgumentError);
	EXPECT_THROW(op.doStuff(&args), ArgumentError);

	args.push_back("a");
	EXPECT_THROW(op.doStuff(nullptr), ArgumentError);
	
}


TEST(CalcTest, PopTest)
{
	ExecutionContext cxt(std::cout);
	Pop op(&cxt);
	std::vector<std::string> args;

	cxt.st.push(12.41);
	cxt.st.push(52.09);

	op.doStuff(nullptr);
	EXPECT_DOUBLE_EQ(cxt.st.top(), 12.41);
	EXPECT_EQ(cxt.st.size(), 1);

	args.push_back("a");
	op.doStuff(&args);
	EXPECT_EQ(cxt.st.size(), 0);
	auto item = cxt.table.find("a");
	EXPECT_NE(item, cxt.table.end());
	EXPECT_DOUBLE_EQ(item->second, 12.41);
}


TEST(CalcTest, PopNegativeTest)
{
	ExecutionContext cxt(std::cout);
	Pop op(&cxt);
	std::vector<std::string> args;
	
	EXPECT_THROW(op.doStuff(nullptr), StackError);

	cxt.st.push(14.1);
	args.push_back("a");
	args.push_back("b");

	EXPECT_THROW(op.doStuff(&args), StackError);
}





int main(int argc, char** argv)
{
	::testing::InitGoogleTest(&argc, argv);

	return RUN_ALL_TESTS();
}
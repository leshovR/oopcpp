#pragma once

#include <string>
#include <vector>
#include "ExecutionContext.h"

class Instruction
{
public:
	Instruction(ExecutionContext* cxt);


	virtual void doStuff(std::vector<std::string>* args = nullptr) = 0;

	virtual ~Instruction();

protected:
	ExecutionContext* context;
};


#include "pch.h"
#include "Calculator.h"

#include "CalcException.h"
#include <string>
#include <sstream>

using namespace std;

Calculator::Calculator(istream& is, ostream& os)
	: in(is), context(os), factory(&context)
{

}


void Calculator::initFactory(InstructionFactory& factory)
{
	factory.registerInst<Push>("PUSH");
	factory.registerInst<Define>("DEFINE");
	factory.registerInst<Add>("ADD");
	factory.registerInst<Sqrt>("SQRT");
	factory.registerInst<Sub>("SUB");
	factory.registerInst<Mul>("MUL");
	factory.registerInst<Div>("DIV");
	factory.registerInst<Pop>("POP");
	factory.registerInst<Print>("PRINT");
	factory.registerInst<Comment>("#");
}


void Calculator::runProgram()
{
	initFactory(factory);

	string line;
	while (getline(in, line))
	{
		string name;
		string word;
		istringstream is(line);
		vector<string> args;
		is >> name;

		while (is >> word)
			args.push_back(word);

		Instruction* inst = factory.createInst(name);
		if (!inst)
		{
			cerr << "Unknown command. Try again" << endl;
			continue;
		}
		try
		{
			inst->doStuff(&args);
		}
		catch (CalcException& ex)
		{
			cerr << ex.what() << endl;
		}

		delete inst;
	}
}
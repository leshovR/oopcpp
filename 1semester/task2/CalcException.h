#pragma once
#include <string>
#include <stdexcept>

class CalcException : public std::logic_error
{
public:
	CalcException(const std::string& msg)
		: logic_error(msg)
	{

	}
};


class ArgumentError : public CalcException
{
public:
	ArgumentError(const std::string& msg)
		: CalcException(msg)
	{

	}
};


class StackError : public CalcException
{
public:
	StackError(const std::string& msg)
		: CalcException(msg)
	{

	}
};

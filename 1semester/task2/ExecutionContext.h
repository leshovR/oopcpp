#pragma once
#include <stack>
#include <map>
#include <string>

struct ExecutionContext
{
	ExecutionContext(std::ostream& str)
		: os(str) { }
	std::stack<double> st;
	std::map<std::string, double> table;
	std::ostream& os;
};
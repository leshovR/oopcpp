#include "FileReader.h"

using namespace std;

bool FileReader::getNextWord(string& word)
{
	char c;

	while (in.get(c))
	{
		if (isalnum(c))
			word.push_back(c);
		else if (word.size() > 0) return true;
	}

	if (word.size() > 0) return true;

	return false;
}

bool FileReader::isEmpty() 
{ 
	return !in; 
}

FileReader::FileReader(istream& input) : in(input) 
{ 

}
#pragma once
#include <string>
#include <vector>
#include <fstream>
#include <map>

class FileWriter
{
public:
	FileWriter(std::map<std::string, unsigned>& text, unsigned words);

	void writeFile(const std::string& filename);

private:
	std::map<std::string, unsigned> contents;
	unsigned wordCount;
};
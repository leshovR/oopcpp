#pragma once

#include "FileReader.h"
#include <map>
#include <string>

class WordCounter
{
public:
	WordCounter(FileReader& freader);

	int getAllWordsCount();

	std::map<std::string, unsigned> countWords();
private:
	FileReader reader;
	std::map<std::string, unsigned> count;
	int wordCount;
};
#include "WordCounter.h"

using std::map;
using std::string;

map<string, unsigned> WordCounter::countWords()
{
	while (!reader.isEmpty())
	{
		string word;
		if (reader.getNextWord(word))
		{
			++count[word];
			++wordCount;
		}
	}

	return count;
}

int WordCounter::getAllWordsCount() 
{ 
	return wordCount; 
}

WordCounter::WordCounter(FileReader& freader) : 
	reader(freader), count(), wordCount(0) 
{

}


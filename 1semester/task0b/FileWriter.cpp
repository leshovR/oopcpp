#include "FileWriter.h"
#include <algorithm>

using namespace std;

void FileWriter::writeFile(const string& filename)
{
	vector<pair<string, unsigned>> words;
	for (auto item : contents)
	{
		words.push_back({ item.first, item.second });
	}

	stable_sort(words.begin(), words.end(), [](const pair<string, unsigned>& a,
		const pair<string, unsigned>& b) { return a.second > b.second; });

	ofstream out(filename);
	for (auto word : words)
	{
		out << word.first << ", " << word.second <<
			", " << double(word.second) / wordCount * 100 << std::endl;
	}
}


FileWriter::FileWriter(map<string, unsigned>& text,
	unsigned words) : contents(text), wordCount(words) 
{

}

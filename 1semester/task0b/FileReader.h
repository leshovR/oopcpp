#pragma once

#include <istream>
#include <string>

class FileReader
{
public:
	FileReader(std::istream& input);

	bool getNextWord(std::string& word);

	bool isEmpty();
private:
	std::string filename;
	std::istream& in;
};

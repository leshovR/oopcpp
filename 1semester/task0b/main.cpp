#include <fstream>
#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>

#include "WordCounter.h"
#include "FileWriter.h"
using namespace std;


int main(int argc, char** argv)
{
	if (argc != 3)
	{
		cerr << "Wrong number of arguments" << endl;
		return -1;
	}
	ifstream in(argv[1]);

	FileReader reader(in);

	WordCounter counter(reader);
	map<string, unsigned> count = counter.countWords();

	
	FileWriter writer(count, counter.getAllWordsCount());
	writer.writeFile(argv[2]);

	return 0;
}
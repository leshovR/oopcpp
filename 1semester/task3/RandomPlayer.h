#pragma once
#include "Player.h"
#include <SFML/Window.hpp>
#include <iostream>

class RandomPlayer : public Player
{
public:
	RandomPlayer(const sf::Vector2f& origin) : Player(origin) { srand(time(0)); }
	bool placeShips(Ship* ship) override;
	void mouseClick(Events::EventDetails* details) override;

	void update(const Cell shotResult) override { }
	void render(sf::RenderWindow* window) override;
	bool shoot(sf::Vector2u* pos) override;

private:
	
};


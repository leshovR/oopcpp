#include "Button.h"

using namespace GameFramework;

Button::Button(const sf::Vector2f& pos)
{
	setup(pos);
}


Button::~Button()
{
}


void Button::setString(const sf::String& string)
{
	label.setString(string);
}


const sf::String& Button::getString()
{
	return label.getString();
}


void Button::setFont(const sf::Font& font)
{
	label.setFont(font);
}


const sf::Font* Button::getFont()
{
	return label.getFont();
}


void Button::setCharacterSize(const unsigned int chSize)
{
	label.setCharacterSize(chSize);
}


const unsigned int Button::getCharacterSize()
{
	return label.getCharacterSize();
}


void Button::setPosition(const sf::Vector2f& pos)
{
	rect.setPosition(pos);
}


const sf::Vector2f& Button::getPosition()
{
	return rect.getPosition();
}


void Button::setSize(const sf::Vector2f& pos)
{
	rect.setSize(pos);
}


const sf::Vector2f& Button::getSize()
{
	return rect.getSize();
}


void Button::setButtonColor(const sf::Color& color)
{
	rect.setFillColor(color);
}

const sf::Color& Button::getButtonColor()
{
	return rect.getFillColor();
}

void Button::setTextColor(const sf::Color& color)
{
	label.setFillColor(color);
}

const sf::Color& Button::getTextColor()
{
	return label.getFillColor();
}


void Button::update()
{
	sf::Vector2f buttonPos = rect.getPosition();
	sf::Vector2f buttonSize = rect.getSize();
	rect.setOrigin(buttonSize.x / 2.0f, buttonSize.y / 2.0f);

	sf::FloatRect textrect = label.getLocalBounds();
	label.setOrigin(textrect.left + textrect.width / 2.0f,
		textrect.top + textrect.height / 2.0f);
	label.setPosition(buttonPos);

}

void Button::draw(sf::RenderWindow* window)
{
	window->draw(rect);
	window->draw(label);
}


void Button::setup(const sf::Vector2f& pos)
{
	font.loadFromFile("Resources/arial.ttf");
	label.setFont(font);


	setPosition(pos);

}

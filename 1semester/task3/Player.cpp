#include "Player.h"

Player::Player(const sf::Vector2f& origin) : 
	ready(false), world(origin) { }


Ship Player::getRandomShip(size_t sz)
{
	int dir = rand() % 2;
	Direction direction = (dir ? Direction::VERTICAL : Direction::HORIZONTAL);
	size_t x, y;

	if (direction == Direction::HORIZONTAL) // Horizontal
	{
		x = rand() % (10 - sz);
		y = rand() % 10;
	}
	else if (direction == Direction::VERTICAL)
	{
		x = rand() % 10;
		y = rand() % (10 - sz);
	}

	Ship ship = { {x, y}, direction, sz };
	int isValid = world.checkCollision(ship);
	if (!isValid)
		return getRandomShip(sz);

	return ship;
}

void Player::setWorldPosition(const sf::Vector2f& position)
{
	world.setPosition(position);
}

Cell Player::getShot(const sf::Vector2u* pos)
{
	return world.getShot(pos);
}

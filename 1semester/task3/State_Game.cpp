#include "State_Game.h"
#include "Framework/StateManager.h"
#include "Framework/Window.h"


using GameFramework::Events::EventManager;
using GameFramework::States::StateType;
using GameFramework::Events::EventDetails;


void State_Game::onCreate() 
{
	auto players = stateMgr->getContext()->players;
	currentPlayer = players.first;
	opponent = players.second;
	opponent->getWorld()->toggleHiddenShips();
	
	currentPlayer->setWorldPosition(sf::Vector2f(50.0f, 100.0f));
	opponent->setWorldPosition(sf::Vector2f(400.0f, 100.0f));
	currentPlayer->setReady(false);
	opponent->setReady(false);

	sf::Vector2u windowSize = stateMgr->getContext()->
		window->getRenderWindow()->getSize();

	lastShot = Cell::None;
	font.loadFromFile("Resources/arial.ttf");
	nextButton.setFont(font);
	
	nextButton.setPosition(sf::Vector2f(windowSize.x / 2.0f, 
		windowSize.y - 100.0f));
	nextButton.setSize(sf::Vector2f(150.0f, 40.0f));
	nextButton.setButtonColor(sf::Color::Red);
	nextButton.setTextColor(sf::Color::Black);
	nextButton.setString("NEXT TURN");
	nextButton.setCharacterSize(20);

	sf::FloatRect rect = text.getLocalBounds();

	text.setOrigin(rect.left + rect.width / 2.0f,
		rect.top + rect.height / 2.0f);
	text.setPosition(windowSize.x / 2.0f, windowSize.y / 2.0f);

	EventManager* evMgr = stateMgr->getContext()->eventManager;
	evMgr->addCallback(StateType::Game, "Key_Escape",
		&State_Game::mainMenu, this);
	evMgr->addCallback(StateType::Game, "Key_P",
		&State_Game::pause, this);
	evMgr->addCallback(StateType::Game, "Mouse_Left",
		&State_Game::mouseClick, this);
}


void State_Game::onDestroy() 
{
	EventManager* evMgr = stateMgr->getContext()->eventManager;
	evMgr->removeCallback(StateType::Game, "Key_Escape");
	evMgr->removeCallback(StateType::Game, "Key_P");
	evMgr->removeCallback(StateType::Game, "Mouse_Left");
}


void State_Game::update(const sf::Time& time) 
{
	if (currentPlayer->lost() || opponent->lost())
	{
		stateMgr->switchTo(StateType::GameOver);
	}
	if (lastShot == Cell::None)
	{
		bool shot = currentPlayer->shoot(&chosenCell);
		if (shot)
		{
			lastShot = opponent->getShot(&chosenCell);
			currentPlayer->update(lastShot);
			if (lastShot != Cell::Miss) // One more shot
			{
				lastShot = Cell::None;
				currentPlayer->setReady(false);
			}
		}
	}
	nextButton.update();
}


void State_Game::draw() 
{
	sf::RenderWindow* window = stateMgr->getContext()->
		window->getRenderWindow();

	currentPlayer->render(window);
	opponent->render(window);
	nextButton.draw(window);
}


void State_Game::mainMenu(EventDetails* details)
{
	stateMgr->switchTo(StateType::MainMenu);
}


void State_Game::pause(EventDetails* details) 
{
	stateMgr->switchTo(StateType::Paused);
}

void State_Game::activate() { }


void State_Game::deactivate() { }


void State_Game::nextButtonClick(Events::EventDetails* details)
{
	sf::Vector2i mousePos = details->mouse;
	sf::Vector2f buttonPos = nextButton.getPosition();
	sf::Vector2f buttonSize = nextButton.getSize();

	float halfX = buttonSize.x / 2.0f;
	float halfY = buttonSize.y / 2.0f;


	if (lastShot != Cell::None &&
		mousePos.x >= buttonPos.x - halfX &&
		mousePos.x <= buttonPos.x + halfX &&
		mousePos.y >= buttonPos.y - halfY &&
		mousePos.y <= buttonPos.y + halfY)
	{
		currentPlayer->setReady(false);

		Player* tmp = currentPlayer;
		currentPlayer = opponent;
		opponent = tmp;

		sf::Vector2f coords = currentPlayer->getWorld()->getPosition();
		currentPlayer->setWorldPosition(opponent->getWorld()->getPosition());
		opponent->setWorldPosition(coords);

		currentPlayer->getWorld()->toggleHiddenShips();
		opponent->getWorld()->toggleHiddenShips();

		lastShot = Cell::None;
	}
}

void State_Game::mouseClick(Events::EventDetails* details)
{
	sf::Vector2f fieldPos = opponent->getWorld()->getPosition();
	int cellSize = opponent->getWorld()->getCellSize();
	
	sf::Vector2f fieldEnd = fieldPos + 
		sf::Vector2f(cellSize * 10, cellSize * 10);

	sf::Vector2i mousePos = details->mouse;

	if (!currentPlayer->isReady() &&
		mousePos.x >= fieldPos.x &&
		mousePos.x <= fieldEnd.x &&
		mousePos.y >= fieldPos.y &&
		mousePos.y <= fieldEnd.y)
	{
		chosenCell.x = (mousePos.x - fieldPos.x) / cellSize;
		chosenCell.y = (mousePos.y - fieldPos.y) / cellSize;
		currentPlayer->setReady(true);
	}

	nextButtonClick(details);
}
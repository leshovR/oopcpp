#pragma once
#include "Framework/BaseState.h"
#include "Framework/EventManager.h"
#include "Button.h"
#include "Player.h"

using namespace GameFramework;

class State_PlayerPick : public States::BaseState
{
public:
	State_PlayerPick(States::StateManager* stateManager) :
		BaseState(stateManager) { }
	

	void onCreate() override;
	void onDestroy() override;
	void update(const sf::Time& time) override;
	void draw() override;
	void activate() override;
	void deactivate() override;

	void mouseClick(Events::EventDetails* details);
	void mainMenu(Events::EventDetails* details);

private:
	Button buttons[3];
	sf::Text label;
	sf::Font font;

	Player** currentPlayer;
};


#pragma once
#include "Player.h"


using namespace GameFramework;

class ManualPlayer : public Player
{
public:
	ManualPlayer(const sf::Vector2f& origin) : Player(origin) { }
	bool placeShips(Ship* ship) override;
	~ManualPlayer();

	void mouseClick(Events::EventDetails* details) override;

	void render(sf::RenderWindow* window) override;
	void update(const Cell shotResult) override { }
	bool shoot(sf::Vector2u* pos) override;

private:

};


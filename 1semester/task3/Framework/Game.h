#pragma once
#include "Window.h"
#include "StateManager.h"

namespace GameFramework
{
	/*
		The class for managing game window
	*/
	class Game {
	public:

		////// Constructors & destructor //////

		Game();
		~Game();

		////// Interface methods ///////

		
		void handleInput();
		void update();
		void render();
		Window* getWindow();

		////// Framerate managing methods //////

		sf::Time getElapsed();
		void restartClock();

		// ...
		void lateUpdate();

	private:
		////// class attributes //////

		// basic window
		Window window;

		sf::Clock clk;
		sf::Time elapsed;
		// ...

		States::SharedContext context;
		States::StateManager stateManager;
	};
}
#pragma once
#include <SFML/Graphics.hpp>
#include "StateType.h"
#include <unordered_map>
#include <functional>

namespace GameFramework
{
	namespace Events
	{

		enum class EventType
		{
			// SFML events

			KeyDown = sf::Event::KeyPressed,
			KeyUp = sf::Event::KeyReleased,
			MButtonDown = sf::Event::MouseButtonPressed,
			MButtonUp = sf::Event::MouseButtonReleased,
			MouseWheel = sf::Event::MouseWheelMoved,
			WindowResized = sf::Event::Resized,
			GainedFocus = sf::Event::GainedFocus,
			LostFocus = sf::Event::LostFocus,
			MouseEntered = sf::Event::MouseEntered,
			MouseLeft = sf::Event::MouseLeft,
			Closed = sf::Event::Closed,
			TextEntered = sf::Event::TextEntered,


			Keyboard = sf::Event::Count + 1,
			Mouse,
			Joystick
		};

		struct EventInfo
		{
			EventInfo() { code = 0; }
			EventInfo(int event) { code = event; }
			int code;
		};


		using Events = std::vector<std::pair<EventType, EventInfo>>;


		struct EventDetails
		{
			EventDetails(const std::string& bindName) :
				name(bindName)
			{
				clear();
			}

			void clear()
			{
				size = sf::Vector2i(0, 0);
				textEntered = 0;
				mouse = sf::Vector2i(0, 0);
				mouseWheelDelta = 0;
				keyCode = -1;
			}

			std::string name;
			sf::Vector2i size;
			sf::Uint32 textEntered;
			sf::Vector2i mouse;
			int mouseWheelDelta;
			int keyCode;
		};


		struct Binding
		{
			Binding(const std::string& bindname)
				:name(bindname), details(bindname), eventCount(0) { }

			void bindEvent(EventType type, EventInfo info = EventInfo()) 
			{
				events.emplace_back(type, info);
			}

			std::string name;
			EventDetails details;
			int eventCount;  // Count of needed events that are happening
			Events events;   // Events that are needed for the binding 
		};


		// Container for bindings. Key is the name of the binding
		using Bindings = std::unordered_map<std::string, Binding*>;

		using CallbackContainer = std::unordered_map<
			std::string, std::function<void(EventDetails*)>>;

		// Name of the callback and the function-callback
		using Callbacks = std::unordered_map<States::StateType, CallbackContainer>;


		class EventManager
		{
		public:
			EventManager();
			~EventManager();

			bool addBinding(Binding* binding);
			bool removeBinding(std::string name);
			void setFocus(const bool& focus) 
			{
				hasFocus = focus;
			}

			template<typename T>
			bool addCallback(States::StateType state, const std::string& name,
				void (T::*func)(EventDetails*), T* instance)
			{
				auto itr = callbacks.emplace(state, CallbackContainer()).first;

				auto temp = std::bind(func, instance,
					std::placeholders::_1); // 1st param - function-member
											// 2nd - object-instance of the function
											// 3rd - argument to the function

				return itr->second.emplace(name, temp).second;
			}

			bool removeCallback(States::StateType state, const std::string& name) 
			{
				auto itr1 = callbacks.find(state);
				if (itr1 == callbacks.end()) return false;

				auto itr2 = itr1->second.find(name);
				if (itr2 == itr1->second.end()) return false;

				itr1->second.erase(name);
				return true;
			}


			void handleEvent(sf::Event& event);
			void update();

			void setCurrentState(const States::StateType& type);


			sf::Vector2i getMousePos(sf::RenderWindow* wind = nullptr) {
				return (wind ? sf::Mouse::getPosition(*wind) :
					sf::Mouse::getPosition());
			}


		private:
			void loadBindings();

			Bindings bindings;
			Callbacks callbacks;
			bool hasFocus;

			States::StateType currentState;
		};

	}
}
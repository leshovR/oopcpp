#include "State_Intro.h"
#include "StateManager.h"
#include "Window.h"


using GameFramework::States::State_Intro;
using GameFramework::States::BaseState;
using GameFramework::Events::EventManager;
using GameFramework::Events::EventDetails;

void State_Intro::onCreate() 
{
	timePassed = 0.0f;

	sf::Vector2u windowSize = stateMgr->getContext()->
		window->getRenderWindow()->getSize();

	introTexture.loadFromFile("Resources/intro.png");
	introSprite.setTexture(introTexture);

	introSprite.setOrigin(introTexture.getSize().x / 2.0f,
		introTexture.getSize().y / 2.0f);
	introSprite.setPosition(windowSize.x / 2.0f, 0);

	font.loadFromFile("Resources/arial.ttf");
	text.setFont(font);
	text.setString({ "Press SPACE to continue" });
	text.setCharacterSize(15);
	text.setFillColor(sf::Color::Black);

	sf::FloatRect textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width / 2.0f,
		textRect.top + textRect.height / 2.0f);
	text.setPosition(windowSize.x / 2.0f, windowSize.y / 2.0f + 30);

	EventManager* evMgr = stateMgr->getContext()->eventManager;
	evMgr->addCallback(StateType::Intro, "Key_Space", 
		&State_Intro::continueIntro, this);
}


void State_Intro::onDestroy() 
{
	EventManager* evMgr = stateMgr->getContext()->eventManager;
	evMgr->removeCallback(StateType::Intro, "Key_Space");
}


void State_Intro::update(const sf::Time& time)
{
	if (timePassed < 5.0f) 
	{
		timePassed += time.asSeconds();
		introSprite.setPosition(introSprite.getPosition().x,
			introSprite.getPosition().y + 48 * (time.asSeconds()));
	}
}


void State_Intro::draw() 
{
	sf::RenderWindow* window = stateMgr->
		getContext()->window->getRenderWindow();

	window->draw(introSprite);
	if (timePassed >= 5.0f)
	{
		window->draw(text);
	}
}


void State_Intro::continueIntro(EventDetails* details) 
{
	if (timePassed >= 5.0f) 
	{
		stateMgr->switchTo(StateType::MainMenu);
		stateMgr->remove(StateType::Intro);
	}
}


void State_Intro::activate() { }


void State_Intro::deactivate() { }
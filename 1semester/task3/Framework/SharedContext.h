#pragma once
#include "../Player.h"

namespace GameFramework
{
	class Window;
	namespace Events
	{
		class EventManager;
	}
	namespace States
	{
		
		struct SharedContext
		{
			SharedContext() : window(nullptr), eventManager(nullptr) { }

			Window* window;
			Events::EventManager* eventManager;
			std::pair<Player*, Player*> players;
		};
	}
}
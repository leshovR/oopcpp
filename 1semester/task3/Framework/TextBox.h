#pragma once

#include <SFML/Graphics.hpp>
#include <string>

namespace GameFramework
{
	class TextBox
	{
	public:
		using MessageContainer = std::vector<std::string>;

		TextBox();
		TextBox(int visible, int charSize, int width, sf::Vector2f screenPos);
		~TextBox();

		void setup(int visible, int charSize, int width, sf::Vector2f screenPos);
		void add(std::string message);
		void clear();

		void render(sf::RenderWindow& window);

	private:
		MessageContainer messages;
		int numVisible;

		sf::RectangleShape backdrop;
		sf::Font font;
		sf::Text content;
	};
}
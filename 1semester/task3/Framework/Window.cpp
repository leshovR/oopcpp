#include "Window.h"

using GameFramework::Window;
using GameFramework::Events::EventDetails;
using GameFramework::Events::EventManager;
using GameFramework::States::StateType;

Window::Window() {
	setup("Window", sf::Vector2u(640, 480));
}


Window::Window(const std::string& title,
	const sf::Vector2u& size) 
{
	setup(title, size);
}


Window::~Window() {
	destroy();
}


void Window::setup(const std::string& title,
	const sf::Vector2u& size)
{
	windowTitle = title;
	windowSize = size;
	fullScreen = false;
	done = false;

	hasFocus = true;
	eventManager.addCallback(StateType(0), "Fullscreen_toggle", 
		&Window::toggleFullScreen, this);
	eventManager.addCallback(StateType(0), "Window_close",
		&Window::close, this);
	
	create();
}

void Window::create() {
	auto style = (fullScreen ? sf::Style::Fullscreen :
		sf::Style::Default);

	window.create({ windowSize.x, windowSize.y, 32 },
		windowTitle, style);
}


void Window::destroy() {
	window.close();
}

void Window::update() {
	sf::Event event;
	while (window.pollEvent(event)) {
		if (event.type == sf::Event::LostFocus) {
			hasFocus = false;
			eventManager.setFocus(false);
		}
		else if (event.type == sf::Event::GainedFocus) {
			hasFocus = true;
			eventManager.setFocus(true);
		}
		
		eventManager.handleEvent(event);
	}
	eventManager.update();
}


void Window::toggleFullScreen(EventDetails* details) {
	fullScreen = !fullScreen;
	destroy();
	create();
}


void Window::beginDraw() {
	window.clear(sf::Color::White);
}


void Window::endDraw() {
	window.display();
}


bool Window::isDone() {
	return done;
}


bool Window::isFullScreen() {
	return fullScreen;
}


bool Window::isFocused() {
	return hasFocus;
}


sf::RenderWindow* Window::getRenderWindow() {
	return &window;
}


sf::Vector2u Window::getWindowSize() {
	return windowSize;
}


EventManager* Window::getEventManager() {
	return &eventManager;
}


void Window::draw(sf::Drawable& drawable) {
	window.draw(drawable);
}


#pragma once

#include <SFML/Graphics.hpp>

namespace GameFramework
{
	namespace States
	{
		class StateManager;

	
		class BaseState
		{
			friend class StateManager;
		public:
			BaseState(StateManager* stateManager) :
				stateMgr(stateManager), transparent(false),
				transcendent(false) { }

			virtual ~BaseState() { }

			virtual void onCreate() = 0;
			virtual void onDestroy() = 0;

			virtual void activate() = 0;
			virtual void deactivate() = 0;

			virtual void update(const sf::Time& time) = 0;
			virtual void draw() = 0;

			void setTransparent(const bool& transparency) 
			{
				transparent = transparency;
			}

			bool isTransparent() { return transparent; }
			
			void setTranscendent(const bool& transcendence) 
			{
				transcendent = transcendence;
			}

			bool isTranscendent() { return transcendent; }

			StateManager* getStateManager() { return stateMgr; }

		protected:
			StateManager* stateMgr;
			bool transparent;
			bool transcendent;
		};
	}
}




#include "EventManager.h"

#include <fstream>
#include <iostream>
#include <sstream>

using namespace GameFramework::Events;
using GameFramework::States::StateType;

EventManager::EventManager() : hasFocus(true) {
	loadBindings();
}

// Delete all the bindings created
EventManager::~EventManager() {
	for (auto& itr : bindings) {
		delete itr.second;
		itr.second = nullptr;
	}
}


void EventManager::setCurrentState(const StateType& type) {
	currentState = type;
}

// Create binding manually
bool EventManager::addBinding(Binding* binding) {
	if (bindings.find(binding->name) != bindings.end())
		return false;

	return bindings.emplace(binding->name, binding).second;
}

// umm. Removes existing binding. You don't need it anymore?
bool EventManager::removeBinding(std::string name) {
	auto itr = bindings.find(name);
	if (itr == bindings.end()) return false;
	delete itr->second;
	bindings.erase(itr);
	return true;
}

// Handle SFML events
/*
	So here we are checking if the SFML event is needed for any binding to call the callback
	or sth.. am not sure
*/
void EventManager::handleEvent(sf::Event& event) {
	for (auto& bindingsItr : bindings) {
		Binding* bind = bindingsItr.second;
		for (auto& eventsItr : bind->events) {
			EventType sfmlEvent = (EventType)event.type;
			if (eventsItr.first != sfmlEvent) continue;

			if (sfmlEvent == EventType::KeyDown ||
				sfmlEvent == EventType::KeyUp)
			{
				if (eventsItr.second.code == event.key.code) {
					// Matching event/keystroke
					// Increase count
					if (bind->details.keyCode != -1) {
						bind->details.keyCode = eventsItr.second.code;
					}
					++(bind->eventCount);
					break;
				}
			}
			else if (sfmlEvent == EventType::MButtonDown ||
				sfmlEvent == EventType::MButtonUp)
			{
				if (eventsItr.second.code == event.mouseButton.button) {
					// Matching event/keystroke
					// Increase count
					bind->details.mouse.x = event.mouseButton.x;
					bind->details.mouse.y = event.mouseButton.y;
					if (bind->details.keyCode != -1) {
						bind->details.keyCode = eventsItr.second.code;
					}
					++(bind->eventCount);
					break;
				}
			}
			else {
				// No need for additional checking
				if (sfmlEvent == EventType::MouseWheel) {
					bind->details.mouseWheelDelta =
						event.mouseWheel.delta;
				}
				else if (sfmlEvent == EventType::WindowResized) {
					bind->details.size.x = event.size.width;
					bind->details.size.y = event.size.height;
				}
				else if (sfmlEvent == EventType::TextEntered) {
					bind->details.textEntered = event.text.unicode;
				}

				++(bind->eventCount);
			}

		}
	}
}


/*
	And here we are updating the list of bindings
	hoping that all conditions (events) are happening
*/
void EventManager::update() {
	if (!hasFocus) return;

	for (auto& bindingsItr : bindings) {
		Binding* bind = bindingsItr.second;
		for (auto& eventsItr : bind->events) {
			switch (eventsItr.first) {

			case (EventType::Keyboard):
				if (sf::Keyboard::isKeyPressed(
					sf::Keyboard::Key(eventsItr.second.code)))
				{
					if (bind->details.keyCode != -1) {
						bind->details.keyCode = eventsItr.second.code;
					}
					++(bind->eventCount);
				}
				break;
			case (EventType::Mouse):
				if (sf::Mouse::isButtonPressed(
					sf::Mouse::Button(eventsItr.second.code)))
				{
					if (bind->details.keyCode != -1) {
						bind->details.keyCode = eventsItr.second.code;
					}
					++(bind->eventCount);
				}
				break;
			case (EventType::Joystick):
				// Up for expansion

				break;
			}
		}

		if (bind->events.size() == bind->eventCount) {
			auto stateCallbacks = callbacks.find(currentState);
			auto otherCallbacks = callbacks.find(StateType(0));

			if (stateCallbacks != callbacks.end()) {
				auto callItr = stateCallbacks->second.find(bind->name);
				if (callItr != stateCallbacks->second.end()) {
					// Pass in information about events
					callItr->second(&bind->details);
				}
			}

			if (otherCallbacks != callbacks.end()) {
				auto callItr = otherCallbacks->second.find(bind->name);
				if (callItr != otherCallbacks->second.end()) {
					// Pass in information about events
					callItr->second(&bind->details);
				}
			}
		}
		// Preparing for the next tick?

		bind->eventCount = 0;
		bind->details.clear();
	}
}

/*
	Load bindings from file keys.cfg
	binding syntax as follows:
		Callback_name event_code:key_code
	where event_code is code of the event in the
	EventType enumeration
	and key_code is the code for keyboard key or mouse key
	or just 0 for nothing
*/
void EventManager::loadBindings() {
	std::string delimiter = ":";
	std::ifstream bindFile;
	bindFile.open("Resources/keys.cfg");
	if (!bindFile.is_open()) {
		std::cout << "! Failed loading keys.cfg." << std::endl;
		return;
	}
	std::string line;
	while (std::getline(bindFile, line)) {
		std::stringstream keystream(line);
		std::string callbackName;
		keystream >> callbackName;

		Binding* bind = new Binding(callbackName);
		while (!keystream.eof()) {
			std::string keyval;
			keystream >> keyval;
			int start = 0;
			int end = keyval.find(delimiter);
			if (end == std::string::npos) {
				delete bind;
				bind = nullptr;
				break;
			}

			EventType type = EventType(std::stoi(
				keyval.substr(start, end - start)));

			int code = stoi(keyval.substr(end + delimiter.length(),
				keyval.find(delimiter, end + delimiter.length())));

			EventInfo eventInfo(code);
			bind->bindEvent(type, eventInfo);
		}

		if (!addBinding(bind)) delete bind;
		bind = nullptr;
	}
	bindFile.close();
}
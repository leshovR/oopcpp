#pragma once
#include "BaseState.h"
#include "EventManager.h"

namespace GameFramework
{
	namespace States
	{
		class State_Paused : public BaseState
		{
		public:
			State_Paused(StateManager* stateManager) : 
				BaseState(stateManager) { }

			void onCreate() override;
			void onDestroy() override;
			void update(const sf::Time& time) override;
			void draw() override;
			void activate() override;
			void deactivate() override;

			void unpause(Events::EventDetails* details);

		private:
			sf::Text text;
			sf::Font font;
			sf::RectangleShape rect;
		};

	}
}
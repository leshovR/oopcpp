#include "Game.h"

using namespace GameFramework;
using States::StateType;
using States::SharedContext;


Game::Game() : window("Game", sf::Vector2u(800, 600)), 
	context(), stateManager(&context) 
{
	context.window = &window;
	context.eventManager = window.getEventManager();
	stateManager.switchTo(StateType::Intro);
}


Game::~Game() { }


void Game::update() 
{
	window.update();
	stateManager.update(elapsed);
}


void Game::render() 
{
	window.beginDraw();
	stateManager.draw();
	// draw here

	window.endDraw();
}


void Game::lateUpdate() 
{
	stateManager.processRequests();
	restartClock();
}

Window* Game::getWindow() 
{
	return &window;
}


void Game::handleInput() 
{

}


sf::Time Game::getElapsed()
{
	return elapsed;
}


void Game::restartClock() 
{
	elapsed = clk.restart();
}
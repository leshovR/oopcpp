#include "TextBox.h"

using GameFramework::TextBox;

TextBox::TextBox() {
	setup(5, 9, 200, sf::Vector2f(0, 0));
}


TextBox::TextBox(int visible, int charSize,
	int width, sf::Vector2f screenPos) 
{
	setup(visible, charSize, width, screenPos);
}


TextBox::~TextBox() {
	clear();
}


void TextBox::setup(int visible, int charSize,
	int width, sf::Vector2f screenPos) 
{
	numVisible = visible;
	sf::Vector2f offset(2.0f, 2.0f);
	font.loadFromFile("Resources/arial.ttf");
	content.setFont(font);
	content.setString("");
	content.setCharacterSize(charSize);
	content.setFillColor(sf::Color::White);
	content.setPosition(screenPos + offset);

	backdrop.setSize(sf::Vector2f(float(width), (visible * charSize * 1.2f)));
	backdrop.setFillColor(sf::Color(90, 90, 90, 90));
	backdrop.setPosition(screenPos);

}


void TextBox::add(std::string message) {
	messages.push_back(message);
	if (messages.size() < 6) return;
	messages.erase(messages.begin());
}


void TextBox::clear() {
	messages.clear();
}


void TextBox::render(sf::RenderWindow& window) {
	std::string contents;
	for (auto &itr : messages) {
		contents.append(itr + "\n");
	}

	if (contents != "") {
		content.setString(contents);
		window.draw(backdrop);
		window.draw(content);
	}
}
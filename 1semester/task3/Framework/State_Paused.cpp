#include "State_Paused.h"
#include "StateManager.h"
#include "Window.h"

using GameFramework::States::State_Paused;
using GameFramework::Events::EventManager;
using GameFramework::States::StateType;
using GameFramework::Events::EventDetails;

void State_Paused::onCreate() 
{
	setTransparent(true);
	
	font.loadFromFile("Resources/arial.ttf");
	text.setFont(font);
	text.setString(sf::String("PAUSED"));
	text.setCharacterSize(14);
	text.setStyle(sf::Text::Bold);

	sf::Vector2u windowSize = stateMgr->
		getContext()->window->getRenderWindow()->getSize();

	sf::FloatRect textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width / 2.0f,
		textRect.top + textRect.height / 2.0f);
	text.setPosition(windowSize.x / 2.0f, windowSize.y / 2.0f);

	rect.setSize(sf::Vector2f(windowSize));
	rect.setPosition(0, 0);
	rect.setFillColor(sf::Color(0, 0, 0, 150));

	EventManager* evMgr = stateMgr->getContext()->eventManager;
	evMgr->addCallback(StateType::Paused, "Key_P",
		&State_Paused::unpause, this);
}


void State_Paused::onDestroy()
{
	EventManager* evMgr = stateMgr->getContext()->eventManager;
	evMgr->removeCallback(StateType::Paused, "Key_P");
}


void State_Paused::draw() 
{
	sf::RenderWindow* wind = stateMgr->
		getContext()->window->getRenderWindow();

	wind->draw(rect);
	wind->draw(text);
}


void State_Paused::update(const sf::Time& time) { }


void State_Paused::unpause(EventDetails* details) 
{
	stateMgr->switchTo(StateType::Game);
}


void State_Paused::activate() { }


void State_Paused::deactivate() { }
#include "State_MainMenu.h"
#include "StateManager.h"
#include "Window.h"

using GameFramework::States::State_MainMenu;
using GameFramework::Events::EventManager;
using GameFramework::Events::EventDetails;

void State_MainMenu::onCreate() 
{
	font.loadFromFile("Resources/arial.ttf");
	text.setFont(font);
	text.setString(sf::String("MAIN MENU:"));
	text.setCharacterSize(18);

	sf::Vector2u windSize = stateMgr->getContext()->
		window->getRenderWindow()->getSize();

	sf::FloatRect textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width / 2.0f,
		textRect.top + textRect.height / 2.0f);
	text.setPosition(windSize.x / 2.0f, 100.0f);

	buttonSize = sf::Vector2f(300.0f, 32.0f);
	buttonPos = sf::Vector2f(windSize.x / 2.0f, 200.0f);
	buttonPadding = 4; // 4px

	std::string items[3];
	items[0] = "PLAY";
	items[1] = "NOTHING YET";
	items[2] = "EXIT";

	for (int i = 0; i < 3; ++i) 
	{
		sf::Vector2f buttonPosition(buttonPos.x, buttonPos.y +
			(i * (buttonSize.y + buttonPadding)));

		rects[i].setSize(buttonSize);
		rects[i].setFillColor(sf::Color::Red);

		rects[i].setOrigin(buttonSize.x / 2.0f,
			buttonSize.y / 2.0f);
		rects[i].setPosition(buttonPosition);

		labels[i].setFont(font);
		labels[i].setString(sf::String(items[i]));
		labels[i].setCharacterSize(12);

		sf::FloatRect rect = labels[i].getLocalBounds();
		labels[i].setOrigin(rect.left + rect.width / 2.0f,
			rect.top + rect.height / 2.0f);
		labels[i].setPosition(buttonPosition);
	}

	EventManager* evMgr = stateMgr->getContext()->eventManager;
	evMgr->addCallback(StateType::MainMenu, "Mouse_Left",
		&State_MainMenu::mouseClick, this);
}


void State_MainMenu::onDestroy() 
{
	EventManager* evMgr = stateMgr->getContext()->eventManager;
	evMgr->removeCallback(StateType::MainMenu, "Mouse_Left");
}


void State_MainMenu::activate() 
{
	if (stateMgr->hasState(StateType::GameOver))
	{
		labels[0].setString("PLAY");
		sf::FloatRect rect = labels[0].getLocalBounds();
		labels[0].setOrigin(rect.left + rect.width / 2.0f,
			rect.top + rect.height / 2.0f);
		stateMgr->remove(StateType::GameOver);
	}
	else if (stateMgr->hasState(StateType::Game) &&
		labels[0].getString() == "PLAY")
	{
		labels[0].setString("RESUME");
		sf::FloatRect rect = labels[0].getLocalBounds();
		labels[0].setOrigin(rect.left + rect.width / 2.0f,
			rect.top + rect.height / 2.0f);
	}
}


void State_MainMenu::mouseClick(EventDetails* details) 
{
	sf::Vector2i mousePos = details->mouse;

	float halfX = buttonSize.x / 2.0f;
	float halfY = buttonSize.y / 2.0f;

	for (int i = 0; i < 3; ++i) 
	{
		if (mousePos.x >= rects[i].getPosition().x - halfX &&
			mousePos.x <= rects[i].getPosition().x + halfX &&

			mousePos.y >= rects[i].getPosition().y - halfY &&
			mousePos.y <= rects[i].getPosition().y + halfY)
		{
			if (i == 0) 
			{
				if (labels[0].getString() == "PLAY")
					stateMgr->switchTo(StateType::PlayerPick);
				else if (labels[0].getString() == "RESUME")
					stateMgr->switchTo(StateType::Game);
			}
			else if (i == 1) 
			{
				// Credits state

				//stateMgr->switchTo(StateType::Placement);
			}
			else if (i == 2) 
			{
				stateMgr->getContext()->window->close();
			}
		}
	}
}


void State_MainMenu::draw() 
{
	sf::RenderWindow* window = stateMgr->getContext()->
		window->getRenderWindow();

	window->draw(text);

	for (int i = 0; i < 3; ++i) 
	{
		window->draw(rects[i]);
		window->draw(labels[i]);
	}
}


void State_MainMenu::update(const sf::Time& time) { }

void State_MainMenu::deactivate() { }
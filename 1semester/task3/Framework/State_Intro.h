#pragma once
#include "BaseState.h"
#include "EventManager.h"

namespace GameFramework
{
	namespace States
	{
		class State_Intro : public BaseState
		{
		public:
			State_Intro(StateManager* stateManager) : 
				BaseState(stateManager) { }

			void onCreate() override;
			void onDestroy() override;
			void update(const sf::Time& time) override;
			void draw() override;
			void activate() override;
			void deactivate() override;

			void continueIntro(Events::EventDetails* details);

		private:
			sf::Texture introTexture;
			sf::Sprite introSprite;
			sf::Text text;
			sf::Font font;
			float timePassed;

		};

	}
}




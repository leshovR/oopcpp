#pragma once

namespace GameFramework
{
	namespace States
	{
		enum class StateType
		{
			Intro = 1,
			MainMenu,
			PlayerPick,
			Placement,
			Game,
			Paused,
			GameOver,
			Credits
		};
	}
}
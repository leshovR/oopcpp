#pragma once
#include "BaseState.h"
#include "EventManager.h"

namespace GameFramework
{
	namespace States
	{
		class State_MainMenu : public BaseState
		{
		public:
			State_MainMenu(StateManager* stateManager) :
				BaseState(stateManager) { }

			void onCreate() override;
			void onDestroy() override;
			void update(const sf::Time& time) override;
			void draw() override;
			void activate() override;
			void deactivate() override;

			void mouseClick(Events::EventDetails* details);

		private:
			sf::Text text;
			sf::Vector2f buttonSize;
			sf::Vector2f buttonPos;
			unsigned int buttonPadding; // padding between buttons

			sf::RectangleShape rects[3];
			sf::Text labels[3];
			sf::Font font;
		};
	}
}
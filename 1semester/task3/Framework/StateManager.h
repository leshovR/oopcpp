#pragma once
#include <vector>
#include <unordered_map>
#include <functional>
#include "BaseState.h"
#include "SharedContext.h"
#include "EventManager.h"

namespace GameFramework
{
	namespace States
	{
		using StateContainer = std::vector<
			std::pair<StateType, BaseState*>>;


		using TypeContainer = std::vector<StateType>;


		using StateFactory = std::unordered_map<
			StateType, std::function<BaseState*(void)>>;


		class StateManager
		{
		public:
			StateManager(SharedContext* sharedContext);
			~StateManager();

			void update(const sf::Time& time);
			void draw();

			void processRequests();
			
			SharedContext* getContext();
			bool hasState(const StateType& type);
			
			// Change the current state to the state with type 'type'
			void switchTo(const StateType& type);
			void remove(const StateType& type);
			
		
		private:
			// Methods

			void createState(const StateType& type);
			void removeState(const StateType& type);


			/*
				This function maps the state type to a function
				that returns a pointer to allocated memory with
				some state class
			*/
			template<typename T>
			void registerState(const StateType& type) {
				stateFactory[type] = [this]() -> BaseState* 
				{ 
					return new T(this);
				};
			}

			// Members

			SharedContext* shared;
			StateContainer states;
			TypeContainer toRemove;
			StateFactory stateFactory;
		};
	}
}





#pragma once
#include <string>
#include <SFML\Graphics.hpp>
#include "EventManager.h"

namespace GameFramework
{
	/*
		Basic window control class
	*/
	class Window 
	{
	public:
		/////// Constructors & destructor ////////

		/*
			By default, the window has a title "Window"
			and its size is 640x480 px
		*/
		Window();

		/*
			 Initialize an empty window with a title in param title
			 and with size from param size
		*/
		Window(const std::string& title, const sf::Vector2u& size);

		// Destroys the window
		~Window();
		
		////// Interface methods ///////

		// Clear the window
		void beginDraw();

		// Display the changes
		void endDraw(); 

		// Process the events
		void update();

		bool isDone();
		bool isFullScreen();
		sf::Vector2u getWindowSize();
		sf::RenderWindow* getRenderWindow();

		bool isFocused();
		Events::EventManager* getEventManager();

		void toggleFullScreen(Events::EventDetails* details);
		void close(Events::EventDetails* details = nullptr) 
		{
			done = true;
		}

	

		// Draw a drawable object on the window
		void draw(sf::Drawable& drawable);

	private:

		/////// Helper methods ///////
		
		/*
			This method helps to initialize the window
			is used by the constructors only
		*/
		void setup(const std::string& title,
			const sf::Vector2u& size);

		// Close the window
		void destroy();

		//Open the window
		void create();

		/////// Attributes ////////

		sf::RenderWindow window;
		sf::Vector2u windowSize;
		std::string windowTitle;
		bool done;
		bool fullScreen;

		Events::EventManager eventManager;
		bool hasFocus;
	};
}
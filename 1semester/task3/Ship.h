#pragma once
#include <SFML/Window.hpp>

enum class Direction { HORIZONTAL, VERTICAL };


struct Ship
{
	sf::Vector2u origin;
	Direction direction;
	size_t size;
};
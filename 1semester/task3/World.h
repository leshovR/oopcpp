#pragma once
#include <vector>
#include <SFML/Graphics.hpp>
#include "Ship.h"


enum class Cell { Empty, Ship, Miss, Hit, ShipDestroyed, None };

class World
{
public:
	World(const sf::Vector2f& origin);

	void update();
	void render(sf::RenderWindow* window);
	void reset();

	void placeShip(Ship ship);
	bool checkCollision(Ship ship);
	Cell getShot(const sf::Vector2u* pos);
	bool lost();

	void printField();

	int getCellSize() { return cellSize; }
	sf::Vector2f getPosition() { return fieldPosition; }
	void setPosition(const sf::Vector2f& position);
	void toggleHiddenShips() { hidden = !hidden; }

private:
	bool checkShip(const sf::Vector2u& pos);

	Cell matrix[10][10] = { Cell::Empty };

	sf::Vertex line[2];
	sf::Text labels[20];
	sf::Font font;

	sf::Vector2f fieldPosition;
	int cellSize;
	int shipsCount;

	bool hidden;
};
#pragma once
#include <SFML/Graphics.hpp>
#include "Framework/EventManager.h"

namespace GameFramework
{

	class Button
	{
	public:
		Button() = default;
		Button(const sf::Vector2f& pos);
		~Button();

		void setString(const sf::String& string);
		const sf::String& getString();
		void setCharacterSize(const unsigned int chSize);
		const unsigned int getCharacterSize();
		void setFont(const sf::Font& font);
		const sf::Font* getFont();

		void setPosition(const sf::Vector2f& pos);
		const sf::Vector2f& getPosition();
		void setButtonColor(const sf::Color& color);
		const sf::Color& getButtonColor();
		void setTextColor(const sf::Color& color);
		const sf::Color& getTextColor();
		void setSize(const sf::Vector2f& pos);
		const sf::Vector2f& getSize();


		void draw(sf::RenderWindow* window);
		void update();
		//void onClick(Events::EventDetails* details);

	private:
		void setup(const sf::Vector2f& pos);

		sf::RectangleShape rect;
		sf::Text label;
		sf::Font font;
	};

}
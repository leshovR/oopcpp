#include "State_GameOver.h"
#include "Framework/StateManager.h"
#include "Framework/Window.h"

void State_GameOver::onCreate()
{
	font.loadFromFile("Resources/arial.ttf");
	label.setFont(font);
	label.setCharacterSize(40);
	auto players = stateMgr->getContext()->players;
	if (players.second->lost())
		label.setString("Player 1 is the winner");
	else if (players.first->lost())
		label.setString("Player 2 is the winner");
	label.setFillColor(sf::Color::Black);
	sf::FloatRect textrect = label.getLocalBounds();
	label.setOrigin(textrect.width / 2.0f, textrect.height / 2.0f);
	sf::Vector2u windowSize = stateMgr->getContext()->
		window->getRenderWindow()->getSize();
	label.setPosition(sf::Vector2f(windowSize.x / 2.0f, windowSize.y / 2.0f));

	stateMgr->remove(States::StateType::Game);
	stateMgr->remove(States::StateType::Placement);
	stateMgr->remove(States::StateType::PlayerPick);

	auto evMgr = stateMgr->getContext()->eventManager;

	evMgr->addCallback(States::StateType::GameOver, "Key_Space",
		&State_GameOver::mainMenu, this);
}
void State_GameOver::onDestroy()
{
	auto evMgr = stateMgr->getContext()->eventManager;
	evMgr->removeCallback(States::StateType::GameOver, "Key_Space");
}

void State_GameOver::update(const sf::Time & time)
{
	
}

void State_GameOver::draw()
{
	sf::RenderWindow* window = stateMgr->getContext()->
		window->getRenderWindow();
	window->draw(label);

}

void State_GameOver::activate()
{
}

void State_GameOver::deactivate()
{
	auto& players = stateMgr->getContext()->players;
	delete players.first;
	delete players.second;

	players.first = players.second = nullptr;
}

void State_GameOver::mainMenu(Events::EventDetails* details)
{
	stateMgr->switchTo(States::StateType::MainMenu);
}

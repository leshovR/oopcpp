#include "World.h"

#include <sstream>

World::World(const sf::Vector2f& origin)
{
	fieldPosition = origin;
	cellSize = 30;
	shipsCount = 4*1 + 3*2 + 2*3 + 1*4;
	hidden = false;

	font.loadFromFile("Resources/arial.ttf");

	for (int i = 0; i < 20; ++i)
	{
		labels[i].setFont(font);

		labels[i].setCharacterSize(15);
		labels[i].setFillColor(sf::Color::Black);

	}

	for (int i = 0; i < 10; ++i)
	{
		std::ostringstream os;
		os << (i + 1);
		labels[i].setString(sf::String(os.str()));

		labels[i].setPosition(fieldPosition.x - 20,
			fieldPosition.y + i * cellSize);
	}

	for (int i = 10, c = 'a'; i < 20; ++i, ++c)
	{
		std::string label;
		label.push_back(c);
		labels[i].setString(sf::String(label));

		labels[i].setPosition(fieldPosition.x + (i - 10) * cellSize,
			fieldPosition.y - 20);
	}

}


void World::placeShip(Ship ship)
{
	size_t x = ship.origin.x;
	size_t y = ship.origin.y;
	if (ship.direction == Direction::VERTICAL) // Vertical
	{
		for (size_t j = 0; j != ship.size; ++j)
			matrix[y + j][x] = Cell::Ship;
	}
	else if (ship.direction == Direction::HORIZONTAL) // Horizontal
	{
		for (size_t j = 0; j != ship.size; ++j)
			matrix[y][x + j] = Cell::Ship;
	}
}

bool World::checkCollision(Ship ship)
{
	const sf::Vector2u pos = ship.origin;
	sf::Vector2u start;
	start.x = (pos.x ? pos.x - 1 : pos.x);
	start.y = (pos.y ? pos.y - 1 : pos.y);

	sf::Vector2u end;
	if (ship.direction == Direction::HORIZONTAL) // horizontal
	{
		if (pos.x + ship.size > 10) return false;
		end.x = start.x + ship.size;
		if (pos.x > 0) end.x += 1;

		end.y = start.y + 2;
		if (pos.y == 0 || end.y == 10) end.y -= 1;
		if (end.x == 10) end.x -= 1;
	}
	else if (ship.direction == Direction::VERTICAL) 
	{
		if (pos.y + ship.size > 10) return false;
		end.y = start.y + ship.size;
		if (pos.y > 0) end.y += 1;

		end.x = start.x + 2;
		if (pos.x == 0 || end.x == 10) end.x -= 1;
		if (end.y == 10) end.y -= 1;
	}
	else throw std::exception("Unknown direction");

	int sum = 0;
	for (size_t y = start.y; y <= end.y; ++y)
	{
		for (size_t x = start.x; x <= end.x; ++x)
		{
			if (matrix[y][x] != Cell::Empty)
				return false;
		}
	}
	return true;
}


bool World::checkShip(const sf::Vector2u& pos)
{
	int x = pos.x;
	int y = pos.y;
	if (matrix[y][x] != Cell::Hit) return false;


	if (y < 9 && matrix[y + 1][x] == Cell::Ship ||
		y > 0 && matrix[y - 1][x] == Cell::Ship)
		return true;


	if (x < 9 && matrix[y][x + 1] == Cell::Ship ||
		x > 0 && matrix[y][x - 1] == Cell::Ship)
		return true;
	bool ret = false;
	matrix[y][x] = Cell::ShipDestroyed;
	if (x > 0 && checkShip(sf::Vector2u(x - 1, y))) ret = true;
	if (x < 9 && checkShip(sf::Vector2u(x + 1, y))) ret = true;
	if (y > 0 && checkShip(sf::Vector2u(x, y - 1))) ret = true;
	if (y < 9 && checkShip(sf::Vector2u(x, y + 1))) ret = true;
	if (ret) matrix[y][x] = Cell::Hit;
	return ret;
}


Cell World::getShot(const sf::Vector2u* pos)
{
	if (pos == nullptr) return Cell::None;

	Cell cell = matrix[pos->y][pos->x];

	if (cell == Cell::Ship)
	{
		matrix[pos->y][pos->x] = Cell::Hit;
		checkShip(*pos);
		--shipsCount;
	}
	else if (cell == Cell::Empty)
	{
		matrix[pos->y][pos->x] = Cell::Miss;
	}
	else return Cell::None;

	return matrix[pos->y][pos->x];
}

#include <iostream>
void World::printField()
{
	for (int i = 0; i != 10; ++i)
	{
		for (int j = 0; j != 10; ++j)
			std::cout << (int)(bool)matrix[i][j] << ' ';

		std::cout << std::endl;
	}
	std::cout << std::endl;
}

void World::setPosition(const sf::Vector2f& position)
{
	fieldPosition = position;

	for (int i = 0; i < 10; ++i)
	{
		labels[i].setPosition(fieldPosition.x - 20,
			fieldPosition.y + i * cellSize);
	}

	for (int i = 10, c = 'a'; i < 20; ++i, ++c)
	{
		labels[i].setPosition(fieldPosition.x + (i - 10) * cellSize,
			fieldPosition.y - 20);
	}
}


void World::render(sf::RenderWindow* window)
{
	for (int i = 0; i < 20; ++i)
	{
		window->draw(labels[i]);
	}

	for (int i = 0; i <= 10; ++i)
	{
		line[0].position = sf::Vector2f(fieldPosition.x + i * cellSize,
			fieldPosition.y);
		line[1].position = sf::Vector2f(fieldPosition.x + i * cellSize,
			fieldPosition.y + 10 * cellSize);
		line[0].color = line[1].color = sf::Color::Black;
		window->draw(line, 2, sf::Lines);
	}

	for (int i = 0; i <= 10; ++i)
	{
		line[0].position = sf::Vector2f(fieldPosition.x,
			fieldPosition.y + i * cellSize);
		line[1].position = sf::Vector2f(fieldPosition.x + 10 * cellSize,
			fieldPosition.y + i * cellSize);
		line[0].color = line[1].color = sf::Color::Black;
		window->draw(line, 2, sf::Lines);
	}

	for (int y = 0; y < 10; ++y)
		for (int x = 0; x < 10; ++x)
		{
			if (matrix[y][x] == Cell::Ship && !hidden)
			{
				sf::RectangleShape rect;
				rect.setPosition(fieldPosition.x + cellSize * x,
					fieldPosition.y + cellSize * y);
				rect.setSize(sf::Vector2f(cellSize, cellSize));
				rect.setFillColor(sf::Color::Color(50, 50, 150, 80));

				window->draw(rect);
			}
			else if (matrix[y][x] == Cell::Miss)
			{
				sf::CircleShape dot;
				dot.setPosition(fieldPosition.x + cellSize * (x + 0.5f),
					fieldPosition.y + cellSize * (y + 0.5f));
				dot.setRadius(8);
				dot.setFillColor(sf::Color::Color(50, 50, 200, 150));
				sf::FloatRect rect = dot.getLocalBounds();
				dot.setOrigin(rect.left + rect.width / 2.0f,
					rect.top + rect.height / 2.0f);

				window->draw(dot);
			}
			else if (matrix[y][x] == Cell::Hit || 
				matrix[y][x] == Cell::ShipDestroyed)
			{

				sf::RectangleShape rect;
				rect.setPosition(fieldPosition.x + cellSize * x,
					fieldPosition.y + cellSize * y);
				rect.setSize(sf::Vector2f(cellSize, cellSize));
				rect.setFillColor(sf::Color::Color(150, 50, 50, 80));

				window->draw(rect);
			}
		}
}

void World::reset()
{
	for (int i = 0; i < 10; ++i)
		for (int j = 0; j < 10; ++j)
			matrix[i][j] = Cell::Empty;
}


void World::update()
{

}


bool World::lost() { return (shipsCount == 0); }
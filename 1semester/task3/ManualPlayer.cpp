#include "ManualPlayer.h"
#include <SFML/Graphics.hpp>




ManualPlayer::~ManualPlayer()
{
}


bool ManualPlayer::placeShips(Ship* ship)
{
	if (ship == nullptr || ship->size == 0) return false;
	bool ret;
	if ((ret = world.checkCollision(*ship)))
		world.placeShip(*ship);
	return ret;
}


void ManualPlayer::render(sf::RenderWindow* window)
{
	world.render(window);
}

bool ManualPlayer::shoot(sf::Vector2u* pos)
{
	return ready;
}


void ManualPlayer::mouseClick(Events::EventDetails* details)
{
	sf::Vector2i mousePos = details->mouse;
	sf::Vector2f fieldPos = world.getPosition();
	int cellSize = world.getCellSize();
	sf::Vector2f fieldEnd = sf::Vector2f(fieldPos.x + cellSize * 10,
		fieldPos.y + cellSize * 10);

	if (mousePos.x >= fieldPos.x &&
		mousePos.x <= fieldEnd.x &&
		mousePos.y >= fieldPos.y &&
		mousePos.y <= fieldEnd.y)
	{
		int col = (mousePos.x - fieldPos.x) / cellSize;
		int row = (mousePos.y - fieldPos.y) / cellSize;

		Ship ship;
		ship.direction = Direction::HORIZONTAL;
		ship.origin = sf::Vector2u(row, col);
		ship.size = 1;
	}

}

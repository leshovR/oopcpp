#include "State_Placement.h"
#include "Framework/StateManager.h"
#include "Framework/Window.h"
#include <sstream>
#include <string>


#include "OptimalPlayer.h"
#include "RandomPlayer.h"
#include "ManualPlayer.h"

using Events::EventManager;
using namespace States;
using GameFramework::Button;

State_Placement::State_Placement(StateManager* stateManager) :
	BaseState(stateManager)
{
}

State_Placement::~State_Placement()
{
}


void State_Placement::onCreate()
{
	reset();

	auto& players = stateMgr->getContext()->players;
	currentPlayer = players.first;

	font.loadFromFile("Resources/arial.ttf");
	int cellSize = currentPlayer->getWorld()->getCellSize();
	int lineSize = cellSize + 10;
	sf::Vector2f shipsOrigin(450.0f, 200.0f);

	for (int i = 0; i < 4; ++i)
	{
		sf::Vector2f curPos(shipsOrigin.x, shipsOrigin.y + lineSize * i);
		ships[i].setPosition(curPos);
		ships[i].setFillColor(sf::Color::Color(50, 50, 150, 80));
		ships[i].setSize(sf::Vector2f(cellSize * (4 - i), cellSize));

		labels[i].setCharacterSize(15);
		labels[i].setFont(font);
		labels[i].setPosition(curPos.x - 30, curPos.y + 10);
		labels[i].setFillColor(sf::Color::Black);
	}


	buttons[0].setString("RESET");
	buttons[1].setString("READY");
	for (int i = 0; i < 2; ++i)
	{
		buttons[i].setPosition(sf::Vector2f(450, 380 + i * 40));
		buttons[i].setSize(sf::Vector2f(80, 20));
		buttons[i].setButtonColor(sf::Color::Red);
		buttons[i].setTextColor(sf::Color::Black);
		buttons[i].setFont(font);
		buttons[i].setCharacterSize(12);
	}

	EventManager* evMgr = stateMgr->getContext()->eventManager;
	evMgr->addCallback(States::StateType::Placement,
		"Key_Escape", &State_Placement::mainMenu, this);
	evMgr->addCallback(States::StateType::Placement,
		"Mouse_Left", &State_Placement::mouseClick, this);
	evMgr->addCallback(States::StateType::Placement,
		"Mouse_Right", &State_Placement::mouseRightClick, this);
}

void State_Placement::reset()
{
	for (int i = 0; i < 4; ++i)
		shipsCount[i] = i + 1;
	chosenShip.size = 0;
	chosenShip.direction = Direction(0);
	chosenShip.origin = sf::Vector2u(0, 0);

	chosenShipRect.setSize(sf::Vector2f(0.0f, 0.0f));
}

void State_Placement::onDestroy()
{
	EventManager* evMgr = stateMgr->getContext()->eventManager;
	evMgr->removeCallback(States::StateType::Placement, "Key_Escape");
	evMgr->removeCallback(States::StateType::Placement, "Mouse_Left");
	evMgr->removeCallback(States::StateType::Placement, "Mouse_Right");
	
}


void State_Placement::update(const sf::Time& time)
{
	bool shipPlaced = false;

	for (int i = 0; i < 2; ++i)
		buttons[i].update();

	shipPlaced = currentPlayer->placeShips(&chosenShip);
	
	if (shipPlaced)
	{
		if (chosenShipRect.getSize() != sf::Vector2f(0.0f, 0.0f))
		{
			int index = 4 - chosenShip.size;
			shipsCount[index] -= 1;
			if (shipsCount[index] < 0)
				shipsCount[index] = 0;
			chosenShip.size = 0;
			chosenShipRect.setSize(sf::Vector2f(0.0f, 0.0f));
		}
		else
		{
			for (int i = 0; i < 4; ++i)
				shipsCount[i] = 0;
		}
	}

	sf::RenderWindow* window = stateMgr->getContext()->
		window->getRenderWindow();
	sf::Vector2i mousePos = sf::Mouse::getPosition(*window);
	
	chosenShipRect.setPosition(sf::Vector2f(mousePos));
}


void State_Placement::draw()
{
	sf::RenderWindow* window = stateMgr->getContext()->
		window->getRenderWindow();
	currentPlayer->render(window);
	
	for (int i = 0; i < 4; ++i)
	{
		labels[i].setString(sf::String("x") + std::to_string(shipsCount[i]));

		window->draw(ships[i]);
		window->draw(labels[i]);
	}
	
	for (int i = 0; i < 2; ++i)
		buttons[i].draw(window);

	window->draw(chosenShipRect);
}


void State_Placement::activate() { }


void State_Placement::deactivate() { }


void State_Placement::mainMenu(Events::EventDetails* details)
{
	stateMgr->switchTo(States::StateType::MainMenu);
}


void State_Placement::startGame(Events::EventDetails* details)
{
	stateMgr->switchTo(States::StateType::Game);
}


void State_Placement::mouseClick(Events::EventDetails* details)
{
	sf::Vector2i mousePos = details->mouse;
	int cellSize = currentPlayer->getWorld()->getCellSize();

	for (int i = 0; i < 4; ++i)
	{
		sf::Vector2f upperLeft = ships[i].getPosition();
		sf::Vector2f bottomRight = ships[i].getPosition() +
			ships[i].getSize();

		if (mousePos.x >= upperLeft.x &&
			mousePos.x <= bottomRight.x &&
			mousePos.y >= upperLeft.y &&
			mousePos.y <= bottomRight.y)
		{
			int shipSize = 4 - i;
			if (shipsCount[i] > 0) // Drag the ship rect copy
			{
				chosenShipRect = ships[i];
				sf::Vector2f origin = chosenShipRect.getOrigin();
				origin.x += cellSize / 2.0f;
				origin.y += cellSize / 2.0f;
				chosenShipRect.setOrigin(origin);
			}
		}
	}

	sf::Vector2f fieldPos = currentPlayer->getWorld()->getPosition();
	sf::Vector2f fieldEnd = { fieldPos.x + cellSize * 10, 
		fieldPos.y + cellSize * 10 };
	if (chosenShipRect.getSize() != sf::Vector2f(0.0f, 0.0f) &&
		mousePos.x >= fieldPos.x &&
		mousePos.x <= fieldEnd.x &&
		mousePos.y >= fieldPos.y &&
		mousePos.y <= fieldEnd.y)
	{
		int shipSizeX = chosenShipRect.getSize().x / cellSize;
		int shipSizeY = chosenShipRect.getSize().y / cellSize;
		int shipSize = (shipSizeX <= shipSizeY ? shipSizeY : shipSizeX);

		chosenShip.size = shipSize;
		chosenShip.direction = (shipSizeX <= shipSizeY ? Direction::VERTICAL :
			Direction::HORIZONTAL);

		chosenShip.origin.x = (mousePos.x - fieldPos.x) / cellSize;
		chosenShip.origin.y = (mousePos.y - fieldPos.y) / cellSize;	
	}

	resetButtonClick(details);
	readyButtonClick(details);
}


void State_Placement::resetButtonClick(Events::EventDetails* details)
{
	sf::Vector2i mousePos = details->mouse;
	sf::Vector2f buttonPos = buttons[0].getPosition();
	sf::Vector2f buttonSize = buttons[0].getSize();
	float halfX = buttonSize.x / 2.0f;
	float halfY = buttonSize.y / 2.0f;


	if (mousePos.x >= buttonPos.x - halfX &&
		mousePos.x <= buttonPos.x + halfX &&
		mousePos.y >= buttonPos.y - halfY &&
		mousePos.y <= buttonPos.y + halfY)
	{
		reset();
		currentPlayer->reset();
	}
}


void State_Placement::readyButtonClick(Events::EventDetails* details)
{
	sf::Vector2i mousePos = details->mouse;
	sf::Vector2f buttonPos = buttons[1].getPosition();
	sf::Vector2f buttonSize = buttons[1].getSize();
	float halfX = buttonSize.x / 2.0f;
	float halfY = buttonSize.y / 2.0f;


	if (mousePos.x >= buttonPos.x - halfX &&
		mousePos.x <= buttonPos.x + halfX &&
		mousePos.y >= buttonPos.y - halfY &&
		mousePos.y <= buttonPos.y + halfY)
	{
		for (int i = 0; i < 4; ++i)
			if (shipsCount[i] != 0) return;
		sf::String label = buttons[1].getString();
		if (label == "READY")
		{
			currentPlayer = stateMgr->getContext()->players.second;
			buttons[1].setString("START");
			reset();
		}
		else if (label == "START")
			startGame(details);
	}
}


void State_Placement::mouseRightClick(Events::EventDetails* details)
{
	sf::Vector2f shipSize = chosenShipRect.getSize();
	chosenShipRect.setSize(sf::Vector2f(shipSize.y, shipSize.x));
	chosenShip.direction = 
		(chosenShip.direction == Direction::HORIZONTAL ?
			Direction::VERTICAL : Direction::HORIZONTAL);
}
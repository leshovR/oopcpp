#pragma once
#include <SFML/Window.hpp>
#include <iostream>
#include "Player.h"

class OptimalPlayer : public Player
{
public:
	OptimalPlayer(const sf::Vector2f& origin);
	bool placeShips(Ship* ship) override;
	void mouseClick(Events::EventDetails* details) override;

	void update(const Cell shotResult) override;
	void render(sf::RenderWindow* window) override;
	bool shoot(sf::Vector2u* pos/*, const Cell lastShot*/) override;

private:
	bool validateShot(const sf::Vector2u& pos);

	Cell enemyField[10][10] = { Cell::Empty }; 
	sf::Vector2u lastShot;

	int enemyShipCount;

	enum class Stage { Seek, Destroy } stage;
};


#pragma once
#include "OptimalPlayer.h"
#include "RandomPlayer.h"
#include "Framework/Window.h"

using GameFramework::Window;

class Game
{
public:
	Game();
	~Game();

	void handleInput();
	void update();
	void render();
	inline Window* getWindow();

	inline sf::Time getElapsed();
	void restartClock();
private:
	Player* p1;

	Window window;
	sf::Clock clk;
	sf::Time elapsed;

};


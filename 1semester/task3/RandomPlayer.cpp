#include "RandomPlayer.h"


bool RandomPlayer::placeShips(Ship* ship)
{
	if (ready) return true;
	

	for (size_t i = 4; i != 0; --i)
	{
		for (size_t k = 0; k != 5 - i; ++k)
		{
			auto ship = getRandomShip(i);
			world.placeShip(ship);
		}
	}

	ready = true;
	return true;
}


void RandomPlayer::render(sf::RenderWindow* window)
{
	world.render(window);
}

bool RandomPlayer::shoot(sf::Vector2u* pos)
{
	if (pos == nullptr) return false;
	
	pos->x = rand() % 10;
	pos->y = rand() % 10;

	return true;
}


void RandomPlayer::mouseClick(Events::EventDetails* details) { }
#pragma once
#include "World.h"
#include "Framework/EventManager.h"

using namespace GameFramework;


class Player
{
public:
	Player(const sf::Vector2f& origin);
	virtual ~Player() { }
	virtual bool placeShips(Ship* ship) = 0;
	Ship getRandomShip(size_t sz);
	bool isReady() { return ready; }
	bool lost() { return world.lost(); }
	World* getWorld() { return &world; }
	void reset() { ready = false; world.reset(); }
	void setReady(bool val) { ready = val; }
	void setWorldPosition(const sf::Vector2f& position);

	Cell getShot(const sf::Vector2u* pos);
	virtual bool shoot(sf::Vector2u* pos/*, const Cell lastShot*/) = 0;

	virtual void render(sf::RenderWindow* window) = 0;
	virtual void update(const Cell shotResult) = 0;
	virtual void mouseClick(Events::EventDetails* details) = 0;
protected:
	bool ready;
	World world;
};
#pragma once
#include "Framework/BaseState.h"
#include "Framework/EventManager.h"
#include "Player.h"
#include "Button.h"

using namespace GameFramework;

class State_Game : public States::BaseState
{
public:
	State_Game(States::StateManager* stateManager) :
		BaseState(stateManager) { }

	void onCreate() override;
	void onDestroy() override;
	void update(const sf::Time& time) override;
	void draw() override;
	void activate() override;
	void deactivate() override;

	void mainMenu(Events::EventDetails* details);
	void pause(Events::EventDetails* details);
	void mouseClick(Events::EventDetails* details);
	void nextButtonClick(Events::EventDetails* details);

private:
	sf::Text text;
	sf::Font font;

	sf::Vector2u chosenCell;
	Player* currentPlayer;
	Player* opponent;
	Button nextButton;

	Cell lastShot;
};


#pragma once
#include "Framework/BaseState.h"
#include "Framework/EventManager.h"

using namespace GameFramework;

class State_GameOver : public States::BaseState
{
public:
	State_GameOver(States::StateManager* stateManager) :
		BaseState(stateManager) { }


	void onCreate() override;
	void onDestroy() override;
	void update(const sf::Time& time) override;
	void draw() override;
	void activate() override;
	void deactivate() override;

	void mainMenu(Events::EventDetails* details);

private:

	sf::Font font;
	
	sf::Text label;
};


#include "OptimalPlayer.h"


OptimalPlayer::OptimalPlayer(const sf::Vector2f& origin) :
	Player(origin)
{
	lastShot.x = 10;
	lastShot.y = 8;
	srand(time(0));
	stage = Stage::Seek;
	enemyShipCount = 20;
	/*col = 0;
	row = 3;
	for (int i = 0; i < 4; ++i)
		enemyShipsCount[i] = 4 - i;*/
}

//
//void tableFill()
//{
//	int t[10][10];
//	int sc[4] = { 4, 3, 2, 1 };
//	for (int y = 0; y < 10; ++y)
//	{
//		for (int x = 0; x < 10; ++x)
//		{
//			for (int i = 1; i < 4; ++i) // useless for size 1 ships
//			{
//				if (y >= i)
//					t[y][x] += sc[i];
//				if (x <= 9 - i)
//					t[y][x] += sc[i];
//				if (y <= 9 - i)
//					t[y][x] += sc[i];
//				if (x >= i)
//					t[y][x] += sc[i];
//			}
//
//		}
//	}
//}
//
//
//void updateTable(sf::Vector2u pos, Cell result)
//{
//	int t[10][10];
//
//	if (result == Cell::ShipDestroyed)
//	{
//
//	}
//}


bool OptimalPlayer::placeShips(Ship* ship)
{
	if (ready) return true;

	world.placeShip({ { 0, 0 }, Direction::HORIZONTAL, 4 });
	for (size_t i = 0; i != 2; ++i)
	{
		world.placeShip({ {5 + i * 3, 0}, Direction::HORIZONTAL, 2 });
	}

	for (size_t i = 0; i != 2; ++i)
	{
		world.placeShip({ {i * 4, 2}, Direction::HORIZONTAL, 3 });
	}

	world.placeShip({ { 8, 2 }, Direction::HORIZONTAL, 2 });

	for (size_t i = 0; i != 4; ++i)
	{
		auto ship = getRandomShip(1);
		world.placeShip(ship);
	}

	ready = true;
	return true;
}

void OptimalPlayer::render(sf::RenderWindow* window)
{
	world.render(window);
}

void OptimalPlayer::update(const Cell shotResult)
{
	if (shotResult == Cell::Hit)
	{
		--enemyShipCount;
		stage = Stage::Destroy;
	}
	else if (shotResult == Cell::ShipDestroyed)
	{
		--enemyShipCount;
		stage = Stage::Seek;
		int x = lastShot.x;
		int y = lastShot.y;

		int dx = 0, dy = 0;
		if ((y > 0 && enemyField[y - 1][x] == Cell::Hit) ||
			(y < 9 && enemyField[y + 1][x] == Cell::Hit))
		{
			dx = 0; dy = 1;
		}
		else if
			((x > 0 && enemyField[y][x - 1] == Cell::Hit) ||
			(x < 9 && enemyField[y][x + 1] == Cell::Hit))
		{
			dx = 1; dy = 0;
		}

		int i = 1;
		while (true)
		{
			int posY = y + i * dy;
			int posX = x + i * dx;
			bool done = false;

			if ((posX < 10 && posY < 10) && enemyField[posY][posX] == Cell::Hit)
				enemyField[posY][posX] = Cell::ShipDestroyed;
			else done = true;
			posY = y - i * dy;
			posX = x - i * dx;

			if ((posX >= 0 && posY >= 0) && enemyField[posY][posX] == Cell::Hit)
				enemyField[posY][posX] = Cell::ShipDestroyed;
			else if (done) break;
			++i;
		}
	}

	if (enemyField[lastShot.y][lastShot.x] == Cell::Empty &&
		lastShot.y < 10 && lastShot.x < 10)
		enemyField[lastShot.y][lastShot.x] = shotResult;
}

bool OptimalPlayer::validateShot(const sf::Vector2u& pos)
{
	int x = pos.x;
	int y = pos.y;
	int sum = 0;

	if (x < 0 || y < 0 || x > 9 || y > 9) return false;
	if (enemyField[y][x] != Cell::Empty) return false;

	sf::Vector2u begin = pos;
	if (begin.x > 0) --begin.x;
	if (begin.y > 0) --begin.y;
	sf::Vector2u end = pos;
	if (end.x < 9) ++end.x;
	if (end.y < 9) ++end.y;

	for (int i = begin.y; i <= end.y; ++i)
		for (int j = begin.x; j <= end.x; ++j)
			sum += int(enemyField[i][j] == Cell::ShipDestroyed);

	return (sum == 0 ? true : false);
}

bool OptimalPlayer::shoot(sf::Vector2u* pos)
{
	if (pos == nullptr) return false;
	if (enemyShipCount == 0) return false;

	if (stage == Stage::Destroy)
	{
		int x = lastShot.x;
		int y = lastShot.y;
		Cell cell = enemyField[y][x];
		if (cell == Cell::ShipDestroyed)
			stage = Stage::Seek;
		else if (cell == Cell::Miss)
		{
			cell = Cell::Hit;
			if (x > 0 && enemyField[y][x - 1] == Cell::Hit)
				x -= 1;
			else if (x < 10 && enemyField[y][x + 1] == Cell::Hit)
				x += 1;
			else if (y > 0 && enemyField[y - 1][x] == Cell::Hit)
				y -= 1;
			else if (y < 10 && enemyField[y + 1][x] == Cell::Hit)
				y += 1;
			else return false;
		}
		if (cell == Cell::Hit)
		{
			int dx = 1;
			int dy = 0;
			pos->x = x;
			pos->y = y;
			for (int i = 0; i < 2; ++i)
			{
				for (int j = 0; j < 2; ++j)
				{
					if (enemyField[y + dy][x + dx] == Cell::Hit)
					{
						// dx == 1 for 0 or dx == -1 for 9
						// turn in opposite direction
						if ((x - dx < 0 || x - dx > 9 || y - dy < 0 || y - dy > 9 ||
							enemyField[y - dy][x - dx] == Cell::Miss))
						{
							while (enemyField[pos->y][pos->x] == Cell::Hit)
							{
								pos->x += dx;
								pos->y += dy;
							}
							//done
							lastShot = *pos;
							if (validateShot(*pos)) return true;
						}
						// no need for turning
						else if (enemyField[y - dy][x - dx] == Cell::Empty)
						{
							pos->x = x - dx;
							pos->y = y - dy;

							lastShot = *pos;
							if (validateShot(*pos)) return true;
						}

					}

					dx = -dx;
					dy = -dy;
				}
				int t = dx;
				dx = dy;
				dy = t;
			}
			int r = 0;
			if (x == 0)
				r = rand() % 2;
			else if (x == 9)
				r = -(rand() % 2);
			else
				r = rand() % 3 - 1;

			pos->x = x + r;
			pos->y = y;
			if (r == 0)
			{
				if (y == 0)
					r = rand() % 2;
				else if (y == 9)
					r = -(rand() % 2);
				else
					r = rand() % 3 - 1;
				pos->y = y + r;
			}

			if (validateShot(*pos))
			{
				lastShot = *pos;
				return true;
			}
			else
			{
				pos->x = pos->y = 0;
			}
			return false;

		}
	}
	else if (stage == Stage::Seek)
	{
		if (enemyShipCount <= 4)
		{
			pos->x = rand() % 10;
			pos->y = rand() % 10;
			lastShot = *pos;
			if (validateShot(*pos)) return true;
			return false;
		}
		do
		{
			pos->y = lastShot.y + 4;
			pos->x = lastShot.x;

			if (pos->y > 9)
			{
				++pos->x;
				pos->y %= 9;
				pos->y %= 4;
			}
			if (pos->x > 9)
				pos->x = 0;
			lastShot = *pos;
		} while (!validateShot(*pos));
		lastShot = *pos;
		return true;
	}
}


void OptimalPlayer::mouseClick(Events::EventDetails* details) { }
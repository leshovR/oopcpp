#include "State_PlayerPick.h"
#include "Framework/StateManager.h"
#include "Framework/Window.h"
#include "RandomPlayer.h"
#include "OptimalPlayer.h"
#include "ManualPlayer.h"

using namespace States;

void State_PlayerPick::onCreate()
{
	sf::Vector2u windowSize = stateMgr->getContext()->
		window->getRenderWindow()->getSize();

	sf::Vector2f buttonPos(float(windowSize.x) / 2.0f, 250.0f);

	font.loadFromFile("Resources/arial.ttf");

	label.setFont(font);
	label.setFillColor(sf::Color::Black);
	label.setCharacterSize(80);
	label.setString("PLAYER 1");

	sf::FloatRect textrect = label.getLocalBounds();
	label.setOrigin(sf::Vector2f(textrect.width / 2.0f,
		textrect.height / 2.0f));
	label.setPosition(sf::Vector2f(windowSize.x / 2.0f, 50.0f));
	
	buttons[0].setString("Random");
	buttons[1].setString("Optimal");
	buttons[2].setString("Manual");

	
	for (int i = 0; i < 3; ++i)
	{
		buttons[i].setFont(font);
		buttons[i].setButtonColor(sf::Color::Red);
		buttons[i].setCharacterSize(40);
		buttons[i].setSize(sf::Vector2f(200.0f, 50.0f));
		buttons[i].setPosition(sf::Vector2f(buttonPos.x, 
			buttonPos.y + 60 * i));
	}

	currentPlayer = nullptr;

	auto evMgr = stateMgr->getContext()->eventManager;
	evMgr->addCallback(StateType::PlayerPick, "Mouse_Left",
		&State_PlayerPick::mouseClick, this);
	evMgr->addCallback(StateType::PlayerPick, "Key_Escape",
		&State_PlayerPick::mainMenu, this);
}

void State_PlayerPick::onDestroy()
{
	auto evMgr = stateMgr->getContext()->eventManager;
	evMgr->removeCallback(StateType::PlayerPick, "Mouse_Left");
	evMgr->removeCallback(StateType::PlayerPick, "Key_Escape");
}

void State_PlayerPick::update(const sf::Time & time)
{
	for (int i = 0; i < 3; ++i)
		buttons[i].update();

	auto& players = stateMgr->getContext()->players;

	if (label.getString() == "PLAYER 1")
		currentPlayer = &players.first;
	else
		currentPlayer = &players.second;

}

void State_PlayerPick::draw()
{
	sf::RenderWindow* window = stateMgr->getContext()->
		window->getRenderWindow();
	for (int i = 0; i < 3; ++i)
		buttons[i].draw(window);
	window->draw(label);
}

void State_PlayerPick::activate()
{
}

void State_PlayerPick::deactivate()
{
	label.setString("PLAYER 1");
}


void State_PlayerPick::mainMenu(Events::EventDetails* details)
{
	stateMgr->switchTo(StateType::MainMenu);
}


void State_PlayerPick::mouseClick(Events::EventDetails* details)
{
	sf::Vector2i mousePos = details->mouse;
	for (int i = 0; i < 3; ++i)
	{
		sf::Vector2f buttonPos = buttons[i].getPosition();
		sf::Vector2f buttonSize = buttons[i].getSize();
		int halfX = buttonSize.x / 2.0f;
		int halfY = buttonSize.y / 2.0f;

		if (mousePos.x >= buttonPos.x - halfX &&
			mousePos.x <= buttonPos.x + halfX &&
			mousePos.y >= buttonPos.y - halfY &&
			mousePos.y <= buttonPos.y + halfY)
		{
			if (i == 0)
			{
				if (*currentPlayer) 
					delete *currentPlayer;
				*currentPlayer = new RandomPlayer(sf::Vector2f(50, 100));
			}
			else if (i == 1)
			{
				if (*currentPlayer)
					delete *currentPlayer;
				*currentPlayer = new OptimalPlayer(sf::Vector2f(50, 100));
			}
			else if (i == 2)
			{
				if (*currentPlayer)
					delete *currentPlayer;
				*currentPlayer = new ManualPlayer(sf::Vector2f(50, 100));
			}
			else return;

			auto str = label.getString();
			if (str == "PLAYER 1")
				label.setString("PLAYER 2");
			else if (str == "PLAYER 2")
				stateMgr->switchTo(StateType::Placement);

			
		}
	}
}

#pragma once
#include "Framework/BaseState.h"
#include "Framework/EventManager.h"
#include "World.h"
#include "Player.h"
#include "Button.h"


using namespace GameFramework;

class State_Placement : public States::BaseState
{
public:
	State_Placement(States::StateManager* stateManager);

	void onCreate() override;
	void onDestroy() override;
	void update(const sf::Time& time) override;
	void draw() override;
	void activate() override;
	void deactivate() override;

	void mainMenu(Events::EventDetails* details);
	void startGame(Events::EventDetails* details);
	void mouseClick(Events::EventDetails* details);
	void mouseRightClick(Events::EventDetails* details);
	void resetButtonClick(Events::EventDetails* details);
	void readyButtonClick(Events::EventDetails* details);

	~State_Placement();

	
private:
	
	void reset();

	sf::Font font;
	sf::Text labels[4];
	
	int shipsCount[4];
	sf::RectangleShape ships[4];
	sf::RectangleShape chosenShipRect;
	Ship chosenShip;

	/*
		Button 0 - reset
		Button 1 - ready
	*/
	Button buttons[2];

	Player* currentPlayer;
};

